﻿using UnityEngine;
using UnityEngine.UI;

namespace Polyglot
{
    public class DownloadPolyglotSheetButton : MonoBehaviour
    {
        [SerializeField]
        private Button button;

        [SerializeField]
        private RectTransform progressbar = null;

        private float startWidth;

        private void Reset()
        {
            button = GetComponent<Button>();
        }

        private void OnEnable()
        {
            StartCoroutine(LocalizationImporter.DownloadCustomSheet(UpdateProgressbar));
            //startWidth = progressbar.sizeDelta.x;
            //button.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(OnClick);
        }

        public void OnClick()
        {
            //StartCoroutine(LocalizationImporter.DownloadCustomSheet(UpdateProgressbar));
        }

        private bool UpdateProgressbar(float progress)
        {
            if (progressbar != null)
            {
                progressbar.sizeDelta = new Vector2(progress * startWidth, progressbar.sizeDelta.y);
            }

            return false;
        }
    }
}
