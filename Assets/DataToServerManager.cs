using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public class Data_PinIngreso
{
    public string pin = "1515132132";
    public string codigoActivacion = "DUXS-GASES";
}

[Serializable]
public class Data_InformacionSimulacion
{
    public string IDParticipante;
    public string IDRecurso;
    public string firstName;
    public string lastName;
}

[Serializable]
public class Data_Reporte
{
    public string category;
    public int attempts;
    public string IDParticipante;
    public int score;
    public int totalScore;
    public string start;
    public string IDRecurso;
}


public class DataToServerManager : MonoBehaviour
{
    public static DataToServerManager Instance { get; private set; }
    private string url_PinIngreso = "https://ia-dx-academia.web.app/loginsim";
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        DontDestroyOnLoad(this);
        Data_PinIngreso pinIngreso = new Data_PinIngreso();
        StartCoroutine(Routine_RequestPin(pinIngreso));
    }
    //IEnumerator Post(string url, string bodyJsonString)
    //{
    //    var request = new UnityWebRequest(url, "POST");
    //    byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
    //    request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
    //    request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
    //    request.SetRequestHeader("Content-Type", "application/json");
    //    yield return request.SendWebRequest();
    //    Debug.Log("Status Code: " + request.responseCode);
    //}
    IEnumerator Routine_RequestPin(Data_PinIngreso data)
    {
        UnityWebRequest request = UnityWebRequest.Post(url_PinIngreso, JsonUtility.ToJson(data));
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError(string.Format("Error: {0}", request.error));
        }

        else
        {
            // Response can be accessed through: request.downloadHandler.text
            Debug.LogError(request.downloadHandler.text);
        }
    }
}
