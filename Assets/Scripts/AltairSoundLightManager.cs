using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltairSoundLightManager : MonoBehaviour
{
    public static AltairSoundLightManager Instance { get; private set; }

    private AltairScreenManager screens;
    private AltairManager altair;
    //Lights
    [SerializeField] LuzAltair luzIzquierda;
    [SerializeField] LuzAltair luzDerecha;
    [SerializeField] Light topLeft_Light;
    [SerializeField] Light topRight_Light;
    [SerializeField] GameObject charging_Light;

    //Sound
    [SerializeField] AudioSource left_AudioSource;
    [SerializeField] AudioSource right_AudioSource;
    [SerializeField] AudioSource button_AudioSource;
    [SerializeField] AudioSource off_AudioSource;
    [SerializeField] AudioSource genericBeep_AudioSource;
    [SerializeField] AudioSource airPump_AudioSource;
    [SerializeField] AudioSource finHermeticTest_Beep_AudioSource;
    [SerializeField] AudioSource bombaTapada_AudioSource;

    //Nuevos Sonidos
    [SerializeField] AudioSource bomba_Inicio_AudioSource;
    [SerializeField] AudioSource bumpTest_Error_Loop_AudioSource;
    [SerializeField] AudioSource alarmaAlta_Loop_AudioSource;

    //Events
    [SerializeField] BotonAbajo botonAbajo;
    [SerializeField] BotonOnOff botonOnOff;
    [SerializeField] BotonArriba botonArriba;

    //Booleans
    public bool HermeticTestBeeps = false;
    private bool AlarmaAlataBeeps = false;

    //Clips
    [SerializeField] AudioClip default_Left_Beep;
    [SerializeField] AudioClip default_Right_Beep;
    [SerializeField] AudioClip newHermeticBeepLoop;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        screens = AltairScreenManager.Instance;
        altair = AltairManager.Instance;

        if(altair.isCharging)
        {
            ShowChargingLight(true);
        }

        botonAbajo.OnBotonAbajo.AddListener(ButtonClick);
        botonOnOff.OnBotonOnOff.AddListener(ButtonClick);
        botonArriba.OnBotonArriba.AddListener(ButtonClick);
        altair.OnGrabbedFromChargeStation.AddListener(delegate { ShowChargingLight(false); } );
        screens.OnPassedHermeticTest.AddListener(delegate { PlayHermeticTestPassed();});
        screens.OnPassedBumpTest.AddListener(delegate { luzIzquierda.Apagar(true); });
        //altair.on
        //altair.OnTurnOffHeldDown.AddListener(PlayBeepsBeforeTurnOff);
    }
    private void PlayAudio(AudioSource audioSource)
    {
        audioSource.Play();
    }
    private void StopAudio(AudioSource audioSource)
    {
        audioSource.Stop();
    }
    private void TurnOnLight(Light l)
    {
        l.enabled = true;
    }
    private void TurnOffLight(Light l)
    {
        l.enabled = false;
    }
    private void ButtonClick()
    {
        if(altair.isOn)
        {
            PlayAudio(button_AudioSource);
        }
    }
    public void PlayStartingSequence()
    {
        StartCoroutine(StartSequence());
    }
    IEnumerator StartSequence()
    {
        PlayAudio(left_AudioSource);
        TurnOnLight(topLeft_Light);
        luzDerecha.PrenderRoja();
        luzIzquierda.Apagar(false);
        yield return new WaitForSeconds(0.3f);
        PlayAudio(right_AudioSource);
        TurnOnLight(topRight_Light);
        TurnOffLight(topLeft_Light);
        luzIzquierda.PrenderRoja();
        luzDerecha.Apagar(false);
        yield return new WaitForSeconds(0.3f);
        TurnOffLight(topRight_Light);
        luzIzquierda.Apagar(false);
        luzDerecha.Apagar(false);
    }
    public void PlayOffSound()
    {
        if (off_AudioSource.isPlaying) return;
        PlayAudio(off_AudioSource);
    }
    public void StopOffSound()
    {
        if (!off_AudioSource.isPlaying) return;
        StopAudio(off_AudioSource);
    }

    public void PlayBumpTestFailSound(bool state)
    {
        if (state)
        {
            PlayAudio(bumpTest_Error_Loop_AudioSource);
            AlarmaAlataBeeps = true;
            StartCoroutine(AlarmaAltaLoop());
        }
        else
        {
            AlarmaAlataBeeps = false;
            StopAudio(bumpTest_Error_Loop_AudioSource);
        }
    }

    public void PlayBombaInicioSound(bool state)
    {
        if (state)
        {
            PlayAudio(bomba_Inicio_AudioSource);
        }
        else
        {
            StopAudio(bomba_Inicio_AudioSource);
        }
    }

    public void PlayPumpSound(bool state)
    {
        if(state)
        {
            PlayAudio(airPump_AudioSource);
        }
        else
        {
            StopAudio(airPump_AudioSource);
        }
    }

    public void PlayHermeticTestBeeps(bool state)
    {
        if(state)
        {
            HermeticTestBeeps = true;
            left_AudioSource.loop = true;
            right_AudioSource.loop = true;
            left_AudioSource.clip = newHermeticBeepLoop;
            right_AudioSource.clip = newHermeticBeepLoop;
            StartCoroutine(AlarmasPruebaHermeticidad());
            StartCoroutine(SonidosPruebaHermeticidad());
        }
        else
        {
            left_AudioSource.clip = default_Left_Beep;
            right_AudioSource.clip = default_Right_Beep;
            left_AudioSource.loop = false;
            right_AudioSource.loop = false;
            HermeticTestBeeps = false;
            StopAudio(left_AudioSource);
            StopAudio(right_AudioSource);
            TurnOffLight(topLeft_Light);
            TurnOffLight(topRight_Light);
        }
    }
    
    public void PlayAlarmaAlta(bool state)
    {
        if (state)
        {
            AlarmaAlataBeeps = true;
            //left_AudioSource.loop = true;
            //right_AudioSource.loop = true;
            alarmaAlta_Loop_AudioSource.loop = true;
            PlayAudio(alarmaAlta_Loop_AudioSource);
            StartCoroutine(AlarmaAltaLoop());
        }
        else
        {
            //left_AudioSource.loop = false;
            //right_AudioSource.loop = false;
            alarmaAlta_Loop_AudioSource.loop = false;
            StopAudio(alarmaAlta_Loop_AudioSource);
            AlarmaAlataBeeps = false;
            StopAudio(left_AudioSource);
            StopAudio(right_AudioSource);
            TurnOffLight(topLeft_Light);
            TurnOffLight(topRight_Light);
            luzIzquierda.Apagar(true);
            luzDerecha.Apagar(false);
        }
    }

    private IEnumerator AlarmaAltaLoop()
    {
        //PlayAudio(left_AudioSource);
        //PlayAudio(right_AudioSource);
        TurnOnLight(topLeft_Light);
        TurnOnLight(topRight_Light);
        luzIzquierda.PrenderRoja();
        luzDerecha.Apagar(false);
        yield return new WaitForSeconds(0.5f);
        //StopAudio(left_AudioSource);
        //StopAudio(right_AudioSource);
        TurnOffLight(topLeft_Light);
        TurnOffLight(topRight_Light);
        luzIzquierda.Apagar(false);
        luzDerecha.PrenderRoja();
        yield return new WaitForSeconds(0.5f);
        if (AlarmaAlataBeeps)
        {
            StartCoroutine(AlarmaAltaLoop());
        }
        else
        {
            luzIzquierda.Apagar(true);
            luzDerecha.Apagar(false);
        }
    }

    public void PlayHermeticTestPassed()
    {
        StartCoroutine(HermeticTestPassedBeeps());
    }

    IEnumerator HermeticTestPassedBeeps()
    {
        PlayAudio(finHermeticTest_Beep_AudioSource);
        yield return new WaitForSeconds(0.5f);
        PlayAudio(finHermeticTest_Beep_AudioSource);
        yield return new WaitForSeconds(0.5f);
        PlayAudio(finHermeticTest_Beep_AudioSource);
    }

    IEnumerator SonidosPruebaHermeticidad()
    {
        PlayAudio(left_AudioSource);
        PlayAudio(right_AudioSource);
        yield return new WaitForSeconds(1f);
        StopAudio(left_AudioSource);
        StopAudio(right_AudioSource);
        //yield return new WaitForSeconds(1f);
        if (HermeticTestBeeps)
        {
            StartCoroutine(SonidosPruebaHermeticidad());
        }
    }

    IEnumerator AlarmasPruebaHermeticidad()
    {
        TurnOnLight(topLeft_Light);
        TurnOnLight(topRight_Light);
        luzIzquierda.PrenderRoja();
        luzDerecha.PrenderRoja();
        yield return new WaitForSeconds(1f);
        TurnOffLight(topLeft_Light);
        TurnOffLight(topRight_Light);
        luzIzquierda.Apagar(false);
        luzDerecha.Apagar(false);
        yield return new WaitForSeconds(1f);
        if (HermeticTestBeeps)
        {
            StartCoroutine(AlarmasPruebaHermeticidad());
        }
    }

    private void ShowChargingLight(bool state)
    {
        if(state)
        {
            charging_Light.SetActive(true);
        }
        else
        {
            charging_Light.SetActive(false);
        }
    }

    //private void PlayBeepsBeforeTurnOff()
    //{
    //    InvokeRepeating("PlayGenericBeep", 0f, 1f);
    //    StartCoroutine(CancelBeeping());
    //}

    //IEnumerator CancelBeeping()
    //{
    //    yield return new WaitForSeconds(3f);
    //    CancelInvoke();
    //}
    public void PlayGenericBeep()
    {
        PlayAudio(button_AudioSource);
    }

    public void PlayBombaTapada(bool state)
    {
        if(state)
        {
            PlayAudio(bombaTapada_AudioSource);
        }
        else
        {
            StopAudio(bombaTapada_AudioSource);
        }

    }
}
