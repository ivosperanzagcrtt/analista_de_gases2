using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusicManager : MonoBehaviour
{
    public static MenuMusicManager Instance { get; private set; }
    [SerializeField] private AudioSource musicSource;
    [SerializeField] private AudioSource transitionSource;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlayMenuMusic(bool state)
    {
        if(state)
        {
            musicSource.Play();
        }
        else
        {
            musicSource.Stop();
        }
    }

    public void PlayTransition()
    {
        transitionSource.Play();
    }
}
