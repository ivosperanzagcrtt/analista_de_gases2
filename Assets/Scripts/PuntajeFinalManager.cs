using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PuntajeFinalManager : MonoBehaviour
{
    LanguageManager lang;
    ScoreManager score;
    QuestionLoggerManager logger;
    private PlayerManager player;
    //private ScoreManager scoreManager;
    [SerializeField] TextMeshProUGUI logText;

    private void Start()
    {
        lang = LanguageManager.Instance;
        score = ScoreManager.Instance;
        player = PlayerManager.Instance;
        Cursor.lockState = CursorLockMode.None;
        //scoreManager = ScoreManager.Instance;
        logger = QuestionLoggerManager.Instance;
        ImprimirLogRespuestasErroneas();
        DestroyRemainingScripts();
    }

    private void ImprimirLogRespuestasErroneas()
    {
        string display = "";
        logger.AddToIncorrectlyAnsweredLog(" ");
        logger.AddToLog(lang.GetFromCustomSheet("L_FINALSCORE")/*"Puntaje final: "*/ + score.GetPuntaje() + " / 100");
        logger.AddToIncorrectlyAnsweredLog(lang.GetFromCustomSheet("L_FINALSCORE")/*"Puntaje final: "*/ + score.GetPuntaje() + " / 100"); 
        logger.AddToIncorrectlyAnsweredLog(" ");
        foreach (string msg in logger.answeredIncorrectlyLog.log)
        {
            display += msg.ToString() + '\n';
        }
        logText.text = display;
    }

    private void DestroyRemainingScripts()
    {
        if(!player)
        {
            player = PlayerManager.Instance;
        }
        Destroy(player.gameObject);
    }
}
