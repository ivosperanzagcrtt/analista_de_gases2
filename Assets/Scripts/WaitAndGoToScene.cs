using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaitAndGoToScene : MonoBehaviour
{
    public float timeBeforeChangingScene;
    public string sceneName;
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(timeBeforeChangingScene);
        SceneManager.LoadScene(sceneName);
    }
}
