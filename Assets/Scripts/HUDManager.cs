using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using StarterAssets;

public class HUDManager : MonoBehaviour
{
    public static HUDManager Instance { get; private set; }
    QuestionsManager questions;
    LevelManager levelManager;

    [SerializeField] CanvasGroup grabbingCanvasGroup;
    [SerializeField] CanvasGroup goOutsideCanvas;
    [SerializeField] CanvasGroup interactPozoCanvas;
    [SerializeField] CanvasGroup instructionsCanvas;
    [SerializeField] CanvasGroup consignaAfueraCanvas;

    //Screen texts
    [SerializeField] TextMeshProUGUI grabbingText;
    [SerializeField] TextMeshProUGUI goOutsideText;
    [SerializeField] TextMeshProUGUI interactPozoText;

    //Events
    public UnityEvent OnOpenedInstructions;
    public UnityEvent OnClosedInstructions;

    //Player related
    FirstPersonController player;
    PlayerManager playerManager;

    //Buttons
    public Button consignaAfueraButton;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        player = FirstPersonController.Instance;
        playerManager = PlayerManager.Instance;
        questions = QuestionsManager.Instance;
        levelManager = LevelManager.Instance;
        levelManager.OnLoadedOutsideScene.AddListener(delegate { StartCoroutine(waitAndShowConsignaAfuera()); });
        questions.OnQuestionPopup.AddListener(delegate { WaitAndHidePuntoPozo();  HidePuntoPozoCanvas(); HideGoOutsideCanvas(); HideGrabbingScreen(); ShowInstructions(false);});
    }



    private void Show(CanvasGroup c)
    {
        c.alpha = 1f;
    }
    private void Hide(CanvasGroup c)
    {
        c.alpha = 0f;
    }

    private IEnumerator waitAndShowConsignaAfuera()
    {
        yield return new WaitForSeconds(1f);
        ShowConsignaAfuera1(true);
    }

    public void ShowConsignaAfuera1(bool state)
    {
        if (state)
        {
            Show(consignaAfueraCanvas);
            consignaAfueraCanvas.interactable = true;
            consignaAfueraCanvas.blocksRaycasts = true;
            playerManager.detectInput = false;
            player.DetectInput(false);
            player.SetCanMove(false);
            player.SetCursorVisible(true);
        }
        else
        {
            Hide(consignaAfueraCanvas);
            consignaAfueraCanvas.interactable = false;
            consignaAfueraCanvas.blocksRaycasts = false;
            playerManager.detectInput = true;
            player.DetectInput(true);
            player.SetCanMove(true);
            player.SetCursorVisible(false);
        }
    }

    public void ShowInstructions(bool state)
    {
        if(state)
        {
            Show(instructionsCanvas);
            instructionsCanvas.blocksRaycasts = true;
            instructionsCanvas.interactable = true;
            OnOpenedInstructions?.Invoke();
            player.SetCanMove(false);
            player.SetCursorVisible(true);
        }
        else
        {
            Hide(instructionsCanvas);
            instructionsCanvas.blocksRaycasts = false;
            instructionsCanvas.interactable = false;
            OnClosedInstructions?.Invoke();
            player.SetCanMove(true);
            player.SetCursorVisible(false);

        }
    }

    public void ShowGrabbingScreenWithText(string text)
    {
        grabbingText.text = text;
        Show(grabbingCanvasGroup);
    }
    public void HideGrabbingScreen()
    {
        Hide(grabbingCanvasGroup);
    }

    public void ShowGoOutsideCanvas(string text)
    {
        goOutsideText.text = text;
        Show(goOutsideCanvas);
    }

    public void ShowPuntoPozoCanvas(string text)
    {
        interactPozoText.text = text;
        Show(interactPozoCanvas);
    }

    private void WaitAndHidePuntoPozo()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer) return;
            StartCoroutine(waitAndHide());
            //Canvas.ForceUpdateCanvases();
            //LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
    }

    private IEnumerator waitAndHide()
    {
        yield return new WaitForEndOfFrame();
        Hide(interactPozoCanvas);
        if(interactPozoCanvas.alpha == 1f)
        {
            StartCoroutine(waitAndHide());
        }
    }
    public void HidePuntoPozoCanvas()
    {
        Hide(interactPozoCanvas);
    }

    public void HideGoOutsideCanvas()
    {
        Hide(goOutsideCanvas);
    }
}
