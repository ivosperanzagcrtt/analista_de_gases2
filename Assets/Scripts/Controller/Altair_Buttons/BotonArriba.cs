using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BotonArriba : MonoBehaviour
{
    public UnityEvent OnBotonArriba;
    public UnityEvent OnReleaseBottonArriba;
    QuestionsManager questions;
    private void Start()
    {
        questions = QuestionsManager.Instance;
    }
    private void OnMouseDown()
    {
        if (questions.isOpen) return;
        if (Input.GetMouseButtonDown(0))
        {
            OnBotonArriba?.Invoke();
        }
    }
    private void OnMouseUp()
    {
        OnReleaseBottonArriba?.Invoke();
    }

    private void OnMouseExit()
    {
        OnReleaseBottonArriba?.Invoke();
    }

}
