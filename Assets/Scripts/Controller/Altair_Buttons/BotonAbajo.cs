using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BotonAbajo : MonoBehaviour
{
    public UnityEvent OnBotonAbajo;
    QuestionsManager questions;
    private void Start()
    {
        questions = QuestionsManager.Instance;
    }
    private void OnMouseDown()
    {
        if (questions.isOpen) return;
        if (Input.GetMouseButtonDown(0))
        {
            OnBotonAbajo?.Invoke();
        }
    }
}
