using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BotonOnOff : MonoBehaviour
{
    public UnityEvent OnBotonOnOff;
    public UnityEvent OnReleaseOnOff;
    QuestionsManager questions;
    private void Start()
    {
        questions = QuestionsManager.Instance;
    }

    private void OnMouseDown()
    {
        if (questions.isOpen) return;
        if (Input.GetMouseButtonDown(0))
        {
            OnBotonOnOff?.Invoke();
        }
    }

    private void OnMouseUp()
    {
        OnReleaseOnOff?.Invoke();
    }

    private void OnMouseExit()
    {
        OnReleaseOnOff?.Invoke();
    }
}
