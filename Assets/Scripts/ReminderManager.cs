using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReminderManager : MonoBehaviour
{
    CanvasGroup canvas;
    PlayerManager player;
    AltairManager altair;

    private void Start()
    {
        canvas = GetComponent<CanvasGroup>();
        player = PlayerManager.Instance;
        altair = AltairManager.Instance;
        altair.OnGrabbedFromChargeStation.AddListener(delegate { canvas.alpha = 1f; });
    }

    private void Update()
    {
        HideCheck();
    }
    
    private void HideCheck()
    {
        if(player.showingRightHand && Input.GetKeyDown(KeyCode.Space))
        {
            canvas.alpha = 0f;
            Destroy(this);
        }
    }
}
