using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Events;
using StarterAssets;

public class AltairScreenManager : MonoBehaviour
{
    public static AltairScreenManager Instance { get; private set; }
    public enum OptionScreen { None, Ajuste_Aire_Limpio, Pentano, Bump_Test, PantallaFinal, CalibracionDeCero};
    public OptionScreen optionScreen;
    private LanguageManager language;
    private GasChartManager gasChart;
    private QuestionsManager questions;
    private AltairManager altair;
    private PlayerManager playerManager;
    private AltairSoundLightManager soundLightManager;
    private FirstPersonController firstPersonController;
    private QuestionLoggerManager logger;
    private ZondaAnimationManager zondaAnimation;
    private LevelManager levelManager;

    [SerializeField] private CanvasGroup canvas;

    //Turn on splash
    [SerializeField] private CanvasGroup splashScreen;

    //First screen after boot - Altair Version
    [SerializeField] private CanvasGroup versionScreen;

    //Asking for hermetic test
    [SerializeField] private CanvasGroup hermetic_Test_Screen;

    //After hermeticity test OK
    [SerializeField] private CanvasGroup hermetic_OK_Screen;

    //Tipo de gas Pentano
    [SerializeField] private CanvasGroup tipoDeGasPetano_Screen;

    //Gases Chart
    [SerializeField] private CanvasGroup gasChart_Screen;

    //Fecha
    [SerializeField] private CanvasGroup fecha_Screen;

    //Ult. Cal.
    [SerializeField] private CanvasGroup ultimaCalibracion_Screen;

    //Falta Cal.
    [SerializeField] private CanvasGroup faltaCal_Screen;

    //Porfavor Espere
    [SerializeField] private CanvasGroup porfavor_Espere_Screen;

    //Motion
    [SerializeField] private CanvasGroup motion_Screen;

    //Pantalla Boveda
    [SerializeField] private CanvasGroup boveda_Screen;

    //Pantalla PreHermetic
    [SerializeField] private CanvasGroup preHermetic;

    //Turn off fill screen
    [SerializeField] private CanvasGroup turnOff_Fill;
    [SerializeField] private TextMeshProUGUI percentage_Text;
    [SerializeField] private Image turnOff_FillImage;
    [SerializeField] private TextMeshProUGUI turnOff_Text;
    private bool updatePercentageAndFill;

    //Completely turn off screen
    [SerializeField] private CanvasGroup turnOff_Completly;

    //Pantalla Bump Test FAIL
    [SerializeField] private CanvasGroup bumpTest_FAIL_Screen;

    //Overlays
    [SerializeField] private CanvasGroup Overlay_Top;
    [SerializeField] private CanvasGroup Overlay_Bottom;
    [SerializeField] private CanvasGroup Overlay_Bottom_SI_NO;
    [SerializeField] private CanvasGroup Overlay_Bottom_SoloTexto;
    private bool showingTopOverlay = false;
    private bool showingBotOverlay = false;
    private bool showingOpciones = false;

    //Time and Battery - TOP OVERLAY
    [SerializeField] TextMeshProUGUI time_Text;
    [SerializeField] TextMeshProUGUI title_Text;
    [SerializeField] Image batteryFill;
    [SerializeField] Image BumpTest_CheckMark;

    //Current screen and Load Percentage - BOTTOM OVERLAY
    [SerializeField] TextMeshProUGUI currentScreen_text;
    [SerializeField] TextMeshProUGUI loadPercentage_Text;
    [SerializeField] Image loadPercentageFill;

    //Pantalla Opciones - SI - OPC - NO
    [SerializeField] TextMeshProUGUI option_Text;
    [SerializeField] TextMeshProUGUI leftOption_Text;
    [SerializeField] TextMeshProUGUI middleOption_Text;
    [SerializeField] TextMeshProUGUI rightOption_Text;
    [SerializeField] CanvasGroup leftOption_CanvasGroup;
    [SerializeField] CanvasGroup midOption_CanvasGroup;
    [SerializeField] CanvasGroup rightOption_CanvasGroup;
    private bool titilarTexto_OverlaySiNo = false;

    //Overlay Bottom Solo Texto
    [SerializeField] TextMeshProUGUI bottom_Text;
    private bool titilarTexto_OverlaySoloTexto = false;


    //Event related
    private bool proceedWithHermeticTest = false;
    private bool displayedTrailerQuestions = false;
    private bool answeredFallaBumpTest = false;
    private bool answeredHeartQuestion = false;
    private bool answeredPreHermetic = false;

    //Events
    public UnityEvent OnPassedHermeticTest;
    public UnityEvent OnPassedBumpTest;
    public UnityEvent OnTurnedOffFinalAlarm;

    //Coroutines
    Coroutine turnOffTextCoroutine;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        optionScreen = OptionScreen.None;
        language = LanguageManager.Instance;
        questions = QuestionsManager.Instance;
        altair = AltairManager.Instance;
        playerManager = PlayerManager.Instance;
        soundLightManager = AltairSoundLightManager.Instance;
        gasChart = GasChartManager.Instance;
        firstPersonController = FirstPersonController.Instance;
        logger = QuestionLoggerManager.Instance;
        altair.OnAltairTurnOn.AddListener(delegate { TurnOnSequence(); });
        altair.OnAltairTurnOff.AddListener(delegate { TurnOffSequence(); });
        altair.OnTurnOffHeldDown.AddListener(delegate { TurnOffHeldDownSequence();});
        altair.OnReleasedTurnOff.AddListener(delegate { updatePercentageAndFill = false; if (turnOffTextCoroutine != null) { StopCoroutine(turnOffTextCoroutine); } soundLightManager.StopOffSound(); });
        altair.botonAbajo.OnBotonAbajo.AddListener(delegate { CheckOptionsInput(0); });
        altair.botonOnOff.OnBotonOnOff.AddListener(delegate { CheckOptionsInput(1); });
        altair.botonArriba.OnBotonArriba.AddListener(delegate { CheckOptionsInput(2); });


        //Questions related
        questions.OnAnsweredHermeticQuestions.AddListener(delegate { proceedWithHermeticTest = true; });
        questions.OnAnsweredQ_AlarmaBaja.AddListener(delegate { altair.stopLoadingToAnswer = false; });
        questions.OnAnsweredQ_AlarmaTWA.AddListener(delegate { altair.stopLoadingToAnswer = false; });
        questions.OnAnsweredQ_Calibration.AddListener(delegate { altair.stopLoadingToAnswer = false; });
        questions.OnAnsweredFallaBumpTest.AddListener(delegate { answeredFallaBumpTest = true; });
        questions.OnFinishedAnsweringList.AddListener( delegate { titilarTexto_OverlaySoloTexto = false; });
        questions.OnShowEspacioConfinadoImage.AddListener(delegate { ShowCanvas(boveda_Screen); });
        questions.OnAnsweredCorrectly.AddListener(CheckToCloseBovedaImage);
        questions.OnAnsweredHeartQuestionInside.AddListener(PostPreguntaCorazon);
        questions.OnAnsweredPreHermetic.AddListener(delegate { answeredPreHermetic = true; });
        

        //Loading related events
        altair.OnLoadedPercentage.AddListener(UpdatePercentageLoadedVisually);

        //Boot up Sequence - After hermetic test
        altair.Reached_TipoDeGasPentano.AddListener(ShowTipoDeGasPentano);
        altair.Reached_AlarmaBaja.AddListener(ShowAlarmaBaja);
        altair.Reached_AlarmaAlta.AddListener(ShowAlarmaAlta);
        altair.Reached_AlarmaStel.AddListener(ShowAlarmaStel);
        altair.Reached_AlarmaTWA.AddListener(ShowAlarmaTWA);
        altair.Reached_BotellaCalibracion.AddListener(ShowBotellaCalibracion);
        altair.Reached_FechaScreen.AddListener(ShowFecha);
        altair.Reached_UltCalibracionScreen.AddListener(ShowUltimaCalibracion);
        altair.Reached_FaltaCalibrar.AddListener(ShowFaltaCalibrar);
        altair.Reached_PorFavorEspere.AddListener(ShowPorfavorEspere);
        altair.FinishedLoading.AddListener(CargaCompleta);
        altair.OnHeldDownRightButtonForThreeSeconds.AddListener(ShowCalibracionDesdeCero);
    }

    private void CheckToCloseBovedaImage()
    {
        if (boveda_Screen.alpha == 0f) return;
        HideCanvas(boveda_Screen);
    }

    private void ShowCalibracionDesdeCero()
    {
        if(optionScreen == OptionScreen.Pentano)
        {
            ChangeOptionsText(language.GetFromCustomSheet("A_CAL_DE_CERO")/*"CALIBRACION DE CERO?"*/, 
                language.GetFromCustomSheet("A_YES")/*"SI"*/, 
                language.GetFromCustomSheet("A_NO")/*"NO"*/,
                language.GetFromCustomSheet("ALT_FAS")/* "FAS"*/);
            ShowBottomOverlay_SI_NO(true);
            optionScreen = OptionScreen.CalibracionDeCero;
        }
    }

    private void Update()
    {
        if (!altair.isOn) return;
        if (showingTopOverlay)
        {
            UpdateTimeAndBattery();
        }

        if (updatePercentageAndFill)
        {
            UpdatePercentageAndFillForTurnOff();
        }
        else
        {
            //Hide fill canvas
            HideCanvas(turnOff_Fill);
        }
    }
    public void ShowCanvas(CanvasGroup c)
    {
        c.alpha = 1f;
        c.interactable = true;
        c.blocksRaycasts = true;
    }
    public void HideCanvas(CanvasGroup c)
    {
        c.alpha = 0f;
        c.interactable = true;
        c.blocksRaycasts = true;
    }
    public void TurnOnSequence()
    {
        StartCoroutine(TurnOn());
    }
    IEnumerator TurnOn()
    {
        canvas.alpha = 1f;
        canvas.blocksRaycasts = true;
        canvas.interactable = true;
        ShowCanvas(splashScreen);
        yield return new WaitForSeconds(1f);
        HideCanvas(splashScreen);
        ShowCanvas(versionScreen);
        StartCoroutine(TurnOnSounds());
        //soundLightManager.PlayBombaInicioSound(true);
        ShowTopOverlay(true);
        //yield return new WaitForSeconds(4f);
        //soundLightManager.PlayBombaInicioSound(false);
        //soundLightManager.PlayPumpSound(true);
        //ShowTopOverlay(true);
        StartCoroutine(WaitAndGoToHermeticTestScreen());
    }

    IEnumerator TurnOnSounds()
    {
        soundLightManager.PlayBombaInicioSound(true);
        yield return new WaitForSeconds(4f);
        soundLightManager.PlayBombaInicioSound(false);
        soundLightManager.PlayPumpSound(true);
    }

    IEnumerator WaitAndGoToHermeticTestScreen()
    {
        yield return new WaitForSeconds(8f);
        HideCanvas(versionScreen);
        ShowCanvas(hermetic_Test_Screen);
        soundLightManager.PlayHermeticTestBeeps(true);
        if (!displayedTrailerQuestions)
        {
            displayedTrailerQuestions = true;
            questions.PlayHermeticidadQuestions();
        }
        while (!proceedWithHermeticTest)
        {
            yield return null;
        }

        DoHermeticTest();

    }
    private void DoHermeticTest()
    {
        //if (!proceedWithHermeticTest) return;
        StartCoroutine(WaitAndShowLeftHand());
        soundLightManager.PlayBombaTapada(true);
    }
    IEnumerator WaitAndShowLeftHand()
    {
        yield return new WaitForSeconds(1f);
        altair.ChangeToHermeticTestRotation();
        soundLightManager.PlayPumpSound(false);
        playerManager.SetInputState(false);
        playerManager.lHand = PlayerManager.LeftHandState.HermeticTest;
        playerManager.SetLeftHandVisible(true);
        playerManager.CheckLeftHandState();

        //New question - April 2022
        questions.PlayPreHermetic();
        ShowCanvas(preHermetic);
        while (answeredPreHermetic == false)
        {
            yield return null;
        }
        HideCanvas(preHermetic);
        //

        StartCoroutine(HermeticTestWait());
    }
    IEnumerator HermeticTestWait()
    {
        yield return new WaitForSeconds(4f);
        soundLightManager.PlayHermeticTestBeeps(false);
        playerManager.SetInputState(true);
        playerManager.lHand = PlayerManager.LeftHandState.None;
        playerManager.SetLeftHandVisible(false);
        playerManager.CheckLeftHandState();
        altair.ChangeToStartingRotation();
        HideCanvas(hermetic_Test_Screen);
        ShowCanvas(hermetic_OK_Screen);
        altair.passedHermeticTest = true;
        //altair.canMoveToBelt = true;
        OnPassedHermeticTest?.Invoke();
        AfterHermeticTest();
        //Back to Version screen 
    }
    public void AfterHermeticTest()
    {
        StartCoroutine(PassedHermeticTest());
    }
    IEnumerator PassedHermeticTest()
    {
        soundLightManager.PlayBombaTapada(false);
        yield return new WaitForSeconds(5f);
        soundLightManager.PlayGenericBeep();
        HideCanvas(hermetic_OK_Screen);
        StartCoroutine(TurnOnSounds());
        //soundLightManager.PlayPumpSound(true); //Reemplazar por bomba del principio --------------------------------------------------------------------------------------
        ShowCanvas(versionScreen);
        yield return new WaitForSeconds(7f);
        altair.StartLoading();
    }
    private void UpdatePercentageLoadedVisually()
    {
        float percentage = Mathf.InverseLerp(0f, 100f, altair.loadedPercentage);
        loadPercentageFill.fillAmount = percentage;
        loadPercentage_Text.text = ((int)(percentage * 100)).ToString() + "%";
    }
    public void TurnOffHeldDownSequence()
    {
        StartCoroutine(TurnOffHeldDown());
    }
    IEnumerator turnOffTexts()
    {
        turnOff_Text.text = language.GetFromCustomSheet("A_BM")/*"BOT�N MANTENER"*/;
        yield return new WaitForSeconds(1f);
        turnOff_Text.text = language.GetFromCustomSheet("A_PA")/*"PARA APAGAR"*/;
        yield return new WaitForSeconds(1f);
        StartCoroutine(turnOffTexts());
    }

    IEnumerator TurnOffHeldDown()
    {
        turnOffTextCoroutine = StartCoroutine(turnOffTexts());
        //soundLightManager.PlayPumpSound(false);
        updatePercentageAndFill = true;
        ShowCanvas(turnOff_Fill);
        yield return new WaitForEndOfFrame();
    }
    private void UpdatePercentageAndFillForTurnOff()
    {
        float percentage = Mathf.InverseLerp(0f, altair.shutdownTime, altair.heldDownTimer);
        turnOff_FillImage.fillAmount = percentage;
        percentage_Text.text = ((int)(percentage * 100)).ToString() + "%";
    }
    public void TurnOffSequence()
    {
        StartCoroutine(TurnOff());
    }
    IEnumerator TurnOff()
    {
        StopCoroutine(turnOffTextCoroutine);
        soundLightManager.PlayHermeticTestBeeps(false);
        //soundLightManager.StopAllCoroutines();
        HideAllScreens();
        HideCanvas(turnOff_Fill);
        updatePercentageAndFill = false;
        ShowCanvas(turnOff_Completly);
        yield return new WaitForSeconds(2f);
        HideCanvas(turnOff_Completly);
        canvas.alpha = 0f;
        canvas.blocksRaycasts = false;
        canvas.interactable = false;
    }
    private void HideAllScreens()
    {
        HideCanvas(splashScreen);
        HideCanvas(versionScreen);
        HideCanvas(hermetic_Test_Screen);
        HideCanvas(hermetic_OK_Screen);
    }
    private void UpdateBatteryPercentage()
    {
        batteryFill.fillAmount = Mathf.InverseLerp(0f, 100f, altair.batteryPercentage);
    }
    private void UpdateSystemTime()
    {
        time_Text.text = DateTime.Now.ToString("HH:mm tt");
    }
    private void UpdateTimeAndBattery()
    {
        UpdateBatteryPercentage();
        UpdateSystemTime();
    }
    private void ShowTopOverlay(bool state)
    {
        if (state)
        {
            ShowCanvas(Overlay_Top);
            showingTopOverlay = true;
        }
        else
        {
            HideCanvas(Overlay_Top);
            showingTopOverlay = false;
        }
    }
    private void ShowBottomOverlay(bool state)
    {
        if (state)
        {
            ShowCanvas(Overlay_Bottom);
            showingBotOverlay = true;
        }
        else
        {
            HideCanvas(Overlay_Bottom);
            showingBotOverlay = false;
        }
    }
    private void ShowBottomOverlay_SI_NO(bool state, bool titilar = false)
    {
        if (state)
        {
            ShowCanvas(Overlay_Bottom_SI_NO);
            showingOpciones = true;
            if (titilar)
            {
                StartCoroutine(titilar_Texto_BottomSiNo());
            }
        }
        else
        {
            HideCanvas(Overlay_Bottom_SI_NO);
            showingOpciones = false;
        }
        titilarTexto_OverlaySiNo = true;
    }

    private IEnumerator titilar_Texto_BottomSiNo()
    {
        option_Text.enabled = false;
        yield return new WaitForSeconds(0.5f);
        option_Text.enabled = true;
        yield return new WaitForSeconds(0.5f);
        if (titilarTexto_OverlaySiNo)
        {
            StartCoroutine(titilar_Texto_BottomSiNo());
        }
        else
        {
            HideCanvas(Overlay_Bottom_SoloTexto);
        }
    }

    private void ShowBottomOverlay_SoloTexto(bool state, string textToShow, bool titilar = false)
    {
        if (state)
        {
            ShowCanvas(Overlay_Bottom_SoloTexto);
            bottom_Text.text = textToShow;
            if(titilar)
            {
                StartCoroutine(titilarText_BottomSoloTexto());
            }
        }
        else
        {
            HideCanvas(Overlay_Bottom_SoloTexto);
            bottom_Text.text = "";
        }
        titilarTexto_OverlaySoloTexto = titilar;
    }

    private IEnumerator titilarText_BottomSoloTexto()
    {
        HideCanvas(Overlay_Bottom_SoloTexto);
        yield return new WaitForSeconds(0.5f);
        ShowCanvas(Overlay_Bottom_SoloTexto);
        yield return new WaitForSeconds(0.5f);
        if (titilarTexto_OverlaySoloTexto)
        {
            StartCoroutine(titilarText_BottomSoloTexto());
        }
        else
        {
            HideCanvas(Overlay_Bottom_SoloTexto);
        }
    }

    private void ChangeOptionsText(string title, string leftText, string rightText, string middleText)
    {
        option_Text.text = title;
        if (leftText == "")
        {
            HideCanvas(leftOption_CanvasGroup);
        }
        else
        {
            leftOption_Text.text = leftText;
            ShowCanvas(leftOption_CanvasGroup);
        }

        if (middleText == "")
        {
            HideCanvas(midOption_CanvasGroup);
        }
        else
        {
            middleOption_Text.text = middleText;
            ShowCanvas(midOption_CanvasGroup);
        }

        if (rightText == "")
        {
            HideCanvas(rightOption_CanvasGroup);
        }
        else
        {
            rightOption_Text.text = rightText;
            ShowCanvas(rightOption_CanvasGroup);
        }
        //rightOption_Text.text = rightText;
        //middleOption_Text.text = middleText;
        //leftOption_Text.text = leftText;
    }
    //Boot up Sequence - After hermetic test
    private void ShowTipoDeGasPentano()
    {
        HideCanvas(versionScreen);
        HideCanvas(Overlay_Bottom);
        //HideCanvas(Overlay_Top);
        ShowCanvas(tipoDeGasPetano_Screen);
    }
    private void ShowAlarmaBaja()
    {
        ShowBottomOverlay(true);
        HideCanvas(tipoDeGasPetano_Screen);
        ShowCanvas(gasChart_Screen);
        gasChart.AlarmaBaja(true);
        currentScreen_text.text = language.GetFromCustomSheet("A_AB")/*"ALARMA BAJA"*/;
        altair.stopLoadingToAnswer = true;
        questions.PlayAlarmaBajaQuestions();
    }
    private void ShowAlarmaAlta()
    {
        gasChart.AlarmaBaja(false);
        gasChart.AlarmaAlta(true);
        currentScreen_text.text = language.GetFromCustomSheet("A_AA")/* "ALARMA ALTA"*/;
    }
    private void ShowAlarmaStel()
    {
        gasChart.AlarmaAlta(false);
        gasChart.AlarmaStel(true);
        currentScreen_text.text = language.GetFromCustomSheet("A_AS")/*"ALARMA STEL"*/;
    }
    private void ShowAlarmaTWA()
    {
        gasChart.AlarmaStel(false);
        gasChart.AlarmaTWA(true);
        currentScreen_text.text = language.GetFromCustomSheet("A_TWA")/*"ALARMA TWA"*/;
        altair.stopLoadingToAnswer = true;
        questions.PlayAlarmaTWAQuestions();
    }
    private void ShowBotellaCalibracion()
    {
        gasChart.AlarmaTWA(false);
        gasChart.BotellaCalibracion(true);
        currentScreen_text.text = language.GetFromCustomSheet("A_BCAL")/*"BOTELLA CALIBRACI�N"*/;
        altair.stopLoadingToAnswer = true;
        questions.PlayCalibrationQuestions();
    }
    private void ShowFecha()
    {
        DateTime date = System.DateTime.Today;
        gasChart.BotellaCalibracion(false);
        ShowCanvas(fecha_Screen);
        currentScreen_text.text = date.ToString("MMM") + "-" + date.ToString("dd") + "-" + date.ToString("yy");
    }
    private void ShowUltimaCalibracion()
    {
        DateTime date = System.DateTime.Today.AddDays(-7);
        HideCanvas(fecha_Screen);
        ShowCanvas(ultimaCalibracion_Screen);
        currentScreen_text.text = date.ToString("MMM") + "-" + date.ToString("dd") + "-" + date.ToString("yy");
    }
    private void ShowFaltaCalibrar()
    {
        HideCanvas(ultimaCalibracion_Screen);
        ShowCanvas(faltaCal_Screen);
        currentScreen_text.text = language.GetFromCustomSheet("A_DAYS")/*"32 D�AS"*/;
    }
    private void ShowPorfavorEspere()
    {
        HideCanvas(faltaCal_Screen);
        ShowCanvas(porfavor_Espere_Screen);
        currentScreen_text.text = language.GetFromCustomSheet("A_PLEASE_W")/*"POR FAVOR, ESPERE"*/;
    }
    private void CargaCompleta()
    {
        HideCanvas(gasChart_Screen);
        HideCanvas(porfavor_Espere_Screen);
        ShowBottomOverlay(false);
        currentScreen_text.text = "";
        ShowTopOverlay(false);
        StartCoroutine(WaitBeforeShowingScreen(0.1f));
    }
    IEnumerator WaitBeforeShowingScreen(float time)
    {
        yield return new WaitForSeconds(time);
        ShowTopOverlay(true);
        gasChart.AjusteAireLimpio(true);
        optionScreen = OptionScreen.Ajuste_Aire_Limpio;
        //Show "SI - NO" Options
        ShowCanvas(gasChart_Screen);
        ChangeOptionsText(language.GetFromCustomSheet("A_AJUSTE")/*"AJUSTE AIRE LIMPIO?"*/,
            language.GetFromCustomSheet("A_YES")/*"SI"*/,
            language.GetFromCustomSheet("A_NO")/*"NO"*/, "");
        ShowBottomOverlay_SI_NO(true);
        //Empezar rutina y esperar, si no contesta va directamente a Pre-bump test
        StartCoroutine(waitAndSkipAireLimpio());
    }

    private IEnumerator waitAndSkipAireLimpio()
    {
        yield return new WaitForSeconds(6f);
        if(optionScreen == OptionScreen.Ajuste_Aire_Limpio)
        {
            soundLightManager.PlayGenericBeep();
            gasChart.AireLimpioSatisfactorio(false);
            gasChart.AllGreenAndNormal(true);
            ShowBottomOverlay_SoloTexto(false, "");
            ChangeOptionsText("", "BUMP", "CALIBR", "");
            ShowBottomOverlay_SI_NO(true);
            optionScreen = OptionScreen.Pentano;
        }
    }

    /// <summary>
    /// 0 = Boton Abajo - 1 = Boton On Off - 2 Boton Arriba
    /// </summary>
    /// <param name="index"></param>
    private void CheckOptionsInput(int index)
    {
        if (questions.isOpen) return;
        switch (optionScreen)
        {
            case OptionScreen.None:
                return;
            case OptionScreen.Ajuste_Aire_Limpio:
                CheckOptions_AjusteAireLimpio(index);
                return;
            case OptionScreen.Pentano:
                CheckOptions_Pentano(index);
                return;
            case OptionScreen.Bump_Test:
                CheckOptions_BumpTest(index);
                return;
            case OptionScreen.PantallaFinal:
                CheckOptions_PantallaFinal(index);
                return;
            case OptionScreen.CalibracionDeCero:
                CheckOptions_CalibracionDeCero(index);
                return;
            default:
                Debug.Log("CheckOptionsInput Error");
                return;
        }
    }
    private void CheckOptions_AjusteAireLimpio(int index)
    {
        switch (index)
        {
            case 0:
                altair.loadedPercentage = 0f;
                UpdatePercentageLoadedVisually();
                ShowBottomOverlay_SI_NO(false);
                ShowBottomOverlay(true);
                StartCoroutine(AjusteAireLimpio());
                optionScreen = OptionScreen.None;
                return;
            case 1:
                //Do nothing?
                return;
            case 2:
                //Display warning?
                playerManager.alerts.ShowWarning(language.GetFromCustomSheet("DW_DEBE_FAS")/*"Se debe efectuar la comprobaci�n de FAS."*/);
                logger.AddToLog("");
                logger.AddToLog(language.GetFromCustomSheet("L_PRESSED_NO")/*"<color=#FFD700>El usuario presion� 'NO' en la comprobaci�n de aire limpio.</color>"*/);
                logger.AddToLog("");
                return;
        }
    }
    IEnumerator AjusteAireLimpio()
    {
        while (altair.loadedPercentage <= 99f)
        {
            yield return new WaitForSeconds(.75f);
            altair.loadedPercentage += 15f;
            UpdatePercentageLoadedVisually();
            yield return null;
        }
        //Finished loading
        altair.passedAireLimpio = true;
        soundLightManager.PlayHermeticTestPassed();
        ShowBottomOverlay(false);
        ShowBottomOverlay_SoloTexto(true, language.GetFromCustomSheet("A_AIR_LIMP_SAT")/*"AIRE LIMPIO SATISF."*/);
        gasChart.AjusteAireLimpio(false);
        gasChart.AireLimpioSatisfactorio(true);
        StartCoroutine(WaitAndShowPostAireLimpioSatisfactorio());
        CheckIfPlayerCanGoOutside();
    }
    IEnumerator WaitAndShowPostAireLimpioSatisfactorio()
    {
        yield return new WaitForSeconds(4f);
        soundLightManager.PlayGenericBeep();
        gasChart.AireLimpioSatisfactorio(false);
        gasChart.AllGreenAndNormal(true);
        ShowBottomOverlay_SoloTexto(false, "");
        ChangeOptionsText("", "BUMP", "CALIBR", "");
        ShowBottomOverlay_SI_NO(true);
        optionScreen = OptionScreen.Pentano;
    }
    private void CheckOptions_Pentano(int index)
    {
        switch (index)
        {
            case 0:
                //Change to bump test confirmation screen
                if (playerManager.hasGas)
                {
                    optionScreen = OptionScreen.Bump_Test;
                    gasChart.BeforeBumpTest(true);
                    //Overlay
                    ChangeOptionsText(language.GetFromCustomSheet("ALT_BTQ")/*"BUMP TEST?"*/, language.GetFromCustomSheet("A_LIM")/*"LIMITE"*/,
                        language.GetFromCustomSheet("A_NO")/*"NO"*/,
                        language.GetFromCustomSheet("A_YES")/*"SI"*/);
                    ShowBottomOverlay_SI_NO(true, true);
                }
                else
                {
                    playerManager.alerts.ShowWarning(language.GetFromCustomSheet("DW_DEBE_OBT")/*"Debe obtener el GAS para poder realizar el BUMP TEST."*/);
                }
                return;
            case 1:
                //Do nothing?
                return;
            case 2:
                //Display warning?
                return;
        }
    }
    private void CheckOptions_BumpTest(int index)
    {
        switch (index)
        {
            case 0:
                //PEAK
                return;
            case 1:
                //YES
                StartCoroutine(ExecuteBumpTest());
                titilarTexto_OverlaySiNo = false;
                return;
            case 2:
                //NO
                if (altair.passedBumpTest) return;
                playerManager.alerts.ShowWarning(language.GetFromCustomSheet("DW_DEBE_BT")/*"Se debe efectuar el BUMP TEST."*/);
                logger.AddToLog("");
                logger.AddToLog(language.GetFromCustomSheet("L_NO_BT")/*"<color=#FFD700>El usuario presion� 'NO' en efectuar el BUMP TEST.</color>"*/);
                logger.AddToLog("");
                return;
        }
    }

    private void CheckOptions_PantallaFinal(int index)
    {
        switch (index)
        {
            case 0:
                return;
            case 1:
                return;
            case 2:
                //APAGAR ALARMAA
                soundLightManager.PlayHermeticTestBeeps(false);
                OnTurnedOffFinalAlarm?.Invoke();
                //playerManager.playerController.SetCursorVisible(false);
                playerManager.SetInputState(true);
                playerManager.playerController.SetCanMove(true);
                optionScreen = OptionScreen.None;
                gasChart.AllGreenAndNormal(true);
                WarningMessages.Instance.ShowWarning(language.GetFromCustomSheet("DW_GOOD")/*"Bien, contin�e midiendo."*/);
                return;
        }
    }
    private void CheckOptions_CalibracionDeCero(int index)
    {
        switch (index)
        {
            case 0:
                //SI
                return;
            case 1:
                //FAS
                //Hace aire limpio
                altair.loadedPercentage = 0f;
                UpdatePercentageLoadedVisually();
                ShowBottomOverlay_SI_NO(false);
                ShowBottomOverlay(true);
                StartCoroutine(AjusteAireLimpio());
                optionScreen = OptionScreen.None;
                return;
            case 2:
                //NO
                return;
        }
    }

    IEnumerator ExecuteBumpTest()
    {
        //Altair
        altair.loadedPercentage = 0f;
        UpdatePercentageLoadedVisually();
        gasChart.BeforeBumpTest(false);
        gasChart.BumpTest_1(true);
        ShowBottomOverlay_SI_NO(false);
        currentScreen_text.text = language.GetFromCustomSheet("ALT_BUMP_T")/*"BUMP TEST"*/;
        ShowBottomOverlay(true);

        //Hand
        playerManager.SetLeftHandVisible(false);
        playerManager.lHand = PlayerManager.LeftHandState.HoldGas;
        playerManager.SetLeftHandVisible(true);
        playerManager.CheckLeftHandState();

        //Input
        playerManager.detectInput = false;
        firstPersonController.DetectInput(false);
        while (altair.loadedPercentage <= 35f)
        {
            yield return new WaitForSeconds(.75f);
            altair.loadedPercentage += 7f;
            UpdatePercentageLoadedVisually();
            yield return null;
        }

        //Bump test fail event
        ShowBottomOverlay(false);
        soundLightManager.PlayBumpTestFailSound(true);
        ShowCanvas(bumpTest_FAIL_Screen);
        if(!answeredFallaBumpTest)
        {
            questions.PlayFallBumpTestQuestions();
        }
        while (answeredFallaBumpTest == false)
        {
            yield return null;
        }
        soundLightManager.PlayBumpTestFailSound(false);
        HideCanvas(bumpTest_FAIL_Screen);
        ShowBottomOverlay(true);

        gasChart.BumpTest_1(false);
        gasChart.BumpTest_Passed(true);
        currentScreen_text.text = language.GetFromCustomSheet("A_BT_EXIT")/*"BUMP TEST EXITOSO"*/;
        OnPassedBumpTest?.Invoke();
        yield return new WaitForSeconds(3f); //Despues de esto iria pregunta corazon
        ShowBottomOverlay(false);
        playerManager.SetLeftHandVisible(false);
        if(!answeredHeartQuestion)
        {
            PreguntaCorazon();
        }
        else
        {
            PostPreguntaCorazon();
        }
        //currentScreen_text.text = " ";
        //ChangeOptionsText("", "BUMP", "CALIBR", "");
        //ShowBottomOverlay_SI_NO(true);
        //gasChart.BumpTest_Passed(false);
        //gasChart.AllGreenAndNormal(true);
        //optionScreen = OptionScreen.Pentano;
        //playerManager.SetLeftHandVisible(false); //Removed
        //playerManager.lHand = PlayerManager.LeftHandState.None;
        //playerManager.detectInput = true;
        //firstPersonController.DetectInput(true);
        ////firstPersonController.SetCursorVisible(false);

        ////Altair post
        //altair.passedBumpTest = true;
        //BumpTest_CheckMark.enabled = true;
        //CheckIfPlayerCanGoOutside();
    }

    private void PreguntaCorazon()
    {
        gasChart.ShowHeart(true);
        questions.PlayPreguntasCorazon();
        Question_B();
        gasChart.Question_F(true);
    }

    private void PostPreguntaCorazon() // Wait for event to trigger
    {
        answeredHeartQuestion = true;
        gasChart.ShowHeart(false);
        gasChart.Question_F(false);
        currentScreen_text.text = " ";
        ChangeOptionsText("", "BUMP", language.GetFromCustomSheet("ALT_B_CALB")/*"CALIBR"*/, "");
        ShowBottomOverlay_SI_NO(true);
        gasChart.BumpTest_Passed(false);
        gasChart.AllGreenAndNormal(true);
        optionScreen = OptionScreen.Pentano;
        playerManager.SetLeftHandVisible(false);
        playerManager.lHand = PlayerManager.LeftHandState.None;
        playerManager.detectInput = true;
        firstPersonController.DetectInput(true);
        firstPersonController.SetCursorVisible(false);

        //Altair post
        altair.passedBumpTest = true;
        BumpTest_CheckMark.enabled = true;
        CheckIfPlayerCanGoOutside();
    }

    private void CheckIfPlayerCanGoOutside()
    {
        if (altair.passedHermeticTest && altair.passedBumpTest && altair.passedAireLimpio) // Agregar si hizo lo del corazon 
        {
            playerManager.canGoOutside = true;
            optionScreen = OptionScreen.None;
            //ShowBottomOverlay_SI_NO(false); //False -> Sacas el bump calibr
            ChangeOptionsText("", "BUMP", language.GetFromCustomSheet("ALT_B_CALB")/*"CALIBR"*/, "");
            playerManager.alerts.ShowWarning(language.GetFromCustomSheet("DW_CAN_EXIT")/*"Ya puede salir del trailer."*/);
            title_Text.text = language.GetFromCustomSheet("A_PENT")/*"PENTANO"*/; //Top overlay title
        }
    }

    public void Question_A_B_D()
    {
        ShowBottomOverlay_SoloTexto(true, language.GetFromCustomSheet("A_AA")/*"ALARMA ALTA"*/, true);
        ShowCanvas(gasChart_Screen);
    }
    public void Question_B()
    {
        ShowBottomOverlay_SoloTexto(false, "");
    }
}
