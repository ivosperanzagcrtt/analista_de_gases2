using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateBoundariesWithDelay : MonoBehaviour
{
    private HUDManager hud;
    [SerializeField] List<BoxCollider> colliders;

    private void Start()
    {
        hud = HUDManager.Instance;
        hud.consignaAfueraButton.onClick.AddListener(ActivateColliders);
    }

    private void ActivateColliders()
    {
        for(int i = 0; i < colliders.Count; i++)
        {
            BoxCollider b = colliders[i];
            b.enabled = true;
        }
    }
}
