using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AltairChartSection : MonoBehaviour
{
    public string valueTitle;
    public string innerCircleName;
    public float currentValue = 0f;

    private CanvasGroup canvas;
    [SerializeField] TextMeshProUGUI valueTitle_Text;
    [SerializeField] TextMeshProUGUI value_InnerCircle_Name_Text;
    [SerializeField] TextMeshProUGUI currentValue_Text;

    [SerializeField] private Image chart_Image;
    [SerializeField] private Image greenCheck_Image;

    //Colors
    [SerializeField] Color whiteColor;
    [SerializeField] Color greenColor;
    [SerializeField] Color orangeColor;
    [SerializeField] Color redColor;

    private void Start()
    {
        canvas = GetComponent<CanvasGroup>();
    }
    public void SetValue(float value)
    {
        currentValue = value;
        currentValue_Text.text = currentValue.ToString();
    }

    public void SetValueAsString(string text)
    {
        currentValue_Text.text = text;
    }

    public void ShowValueText(bool state)
    {
        currentValue_Text.gameObject.SetActive(state);
    }

    /// <summary>
    /// 0 = White - 1 = Green - 2 = Orange - 3 = Red 
    /// </summary>
    /// <param name="colorIndex"></param>
    public void SetChartColor(int colorIndex)
    {
        switch(colorIndex)
        {
            case 0:
                chart_Image.color = whiteColor;
                valueTitle_Text.color = Color.black;
                value_InnerCircle_Name_Text.color = Color.black;
                currentValue_Text.color = Color.black;
                return;
            case 1:
                chart_Image.color = greenColor;
                valueTitle_Text.color = Color.black;
                value_InnerCircle_Name_Text.color = Color.black;
                currentValue_Text.color = Color.black;
                return;
            case 2:
                chart_Image.color = orangeColor;
                valueTitle_Text.color = Color.black;
                value_InnerCircle_Name_Text.color = Color.black;
                currentValue_Text.color = orangeColor;
                return;
            case 3:
                chart_Image.color = redColor;
                valueTitle_Text.color = Color.black;
                value_InnerCircle_Name_Text.color = Color.black;
                currentValue_Text.color = redColor;
                return;
            default:
                Debug.Log("No color found!");
                return;
        }
    }
    public void Show(bool state)
    {
        if (!canvas) return;

        if(state)
        {
            canvas.alpha = 1f;
        }
        else
        {
            canvas.alpha = 0f;
        }
        greenCheck_Image.enabled = false;
    }
    public void ShowGreenCheckmark(bool state)
    {
        if(state)
        {
            //Show(false);
            greenCheck_Image.enabled = true;
        }
        else
        {
            //Show(true);
            greenCheck_Image.enabled = false;
        }
    }
}
