using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarmupCoroutine : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(waitAndInitialize());
    }
    private IEnumerator waitAndInitialize()
    {
        yield return new WaitForEndOfFrame();
        Debug.Log("Initialized Coroutines");
    }
}
