using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ZondaAnimationManager : MonoBehaviour
{
    public static ZondaAnimationManager Instance { get; private set; }
    [SerializeField] PreguntasPuntosPozoManager preguntasPozo;
    QuestionsManager questionsManager;
    PlayerManager player;
    [SerializeField] public Animator animator;
    public UnityEvent OnZondaAnimationStarted;
    public UnityEvent OnZondaAnimationLowestPoint;
    public UnityEvent OnZondaAnimationEnded;

    [SerializeField] GameObject zondaParent;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        player = PlayerManager.Instance;
        questionsManager = QuestionsManager.Instance;
    }
    public void ZondaAnimationStarted()
    {
        OnZondaAnimationStarted?.Invoke();
        zondaParent.SetActive(true);
        player.hasZonda = false;
        player.lHand = PlayerManager.LeftHandState.None;
        player.CheckLeftHandState();
        player.detectInput = false;
        player.SetLeftHandVisible(false);
    }

    public void ZondaAnimationLowestPoint()
    {
        OnZondaAnimationLowestPoint?.Invoke();
        PauseAnimator(true);
        StartCoroutine(WaitAndShowQuestion());
    }

    private IEnumerator WaitAndShowQuestion()
    {
        yield return new WaitForSeconds(3f);
        preguntasPozo.PlayPreguntasAdentroBodega();
    }

    public void ZondaAnimationEnded()
    {
        OnZondaAnimationEnded?.Invoke();
        player.hasZonda = true;
        player.lHand = PlayerManager.LeftHandState.Zonda;
        player.CheckLeftHandState();
        player.detectInput = true;
        player.SetLeftHandVisible(true);
        this.gameObject.SetActive(false);
        zondaParent.SetActive(false);
    }

    public void PauseAnimator(bool state)
    {
        if(state)
        {
            animator.speed = 0f;
        }
        else
        {
            animator.speed = 0.5f;
        }
    }
}
