using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestionLoggerManager : MonoBehaviour
{
    public static QuestionLoggerManager Instance { get; private set; }
    public Log dataLog = new Log();
    public Log answeredIncorrectlyLog = new Log();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        dataLog.log = new List<string>();
    }

    public void AddToLog(string text)
    {
        dataLog.log.Add(text);
    }

    public void AddToIncorrectlyAnsweredLog(string text)
    {
        answeredIncorrectlyLog.log.Add(text);
    }

    public void ExportLog()
    {
        string json = JsonUtility.ToJson(dataLog);
        Debug.Log(json);
    }

    [Serializable]
    public class Log
    {
        public List<string> log = new List<string>();
    }

}
