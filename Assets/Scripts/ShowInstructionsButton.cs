using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInstructionsButton : MonoBehaviour
{
    private HUDManager hud;
    [SerializeField] Button openButton;
    [SerializeField] CanvasGroup canvas;

    private void Start()
    {
        hud = HUDManager.Instance;
        hud.OnOpenedInstructions.AddListener(delegate { ShowCanvas(false); });
        hud.OnClosedInstructions.AddListener(delegate { ShowCanvas(true); });
        openButton.onClick.AddListener(ShowInstructions);
    }

    public void ShowInstructions()
    {
        hud.ShowInstructions(true);
    }

    private void ShowCanvas(bool state)
    {
        if(state)
        {
            canvas.alpha = 1f;
            canvas.interactable = true;
            canvas.blocksRaycasts = true;
        }
        else
        {
            canvas.alpha = 0f;
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
        }
    }
}
