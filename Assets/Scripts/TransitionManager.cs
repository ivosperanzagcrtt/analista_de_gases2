using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionManager : MonoBehaviour
{
    public static TransitionManager Instance { get; private set; }
    LevelManager levelManager;
    [SerializeField] CanvasGroup fadeCanvas;
    AltairScreenManager altairScreen;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        levelManager = LevelManager.Instance;
        altairScreen = AltairScreenManager.Instance;
        //altairScreen.OnTurnedOffFinalAlarm.AddListener(FadeOut);
        levelManager.OnLoadedOutsideScene.AddListener(FadeOut);
    }

    public void FadeOut()
    {
        fadeCanvas.alpha = 1f;
        StartCoroutine(fadeOutAnimation());
    }

    IEnumerator fadeOutAnimation()
    {
        while(fadeCanvas.alpha > 0f)
        {
            fadeCanvas.alpha -= 0.005f;
            yield return null;
        }
        yield return new WaitForEndOfFrame();
    }
}
