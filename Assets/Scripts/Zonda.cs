using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Zonda : MonoBehaviour
{
    public static Zonda Instance { get; private set; }
    [SerializeField] Camera mainCamera;
    LanguageManager language;
    LevelManager levelManager;
    TransitionManager transitionManager;
    HUDManager hudManager;
    PlayerManager player;
    PuntosManager puntosManager;
    [SerializeField] private string selectableTag = "PuntoPozo";
    [SerializeField] private Material highlightMaterial;
    [SerializeField] private Material defaultMaterial;
    [SerializeField] private Material barelyVisibleMaterial;
    [SerializeField] float distance;
    [SerializeField] Transform puntoFin;
    [SerializeField] KeyCode interactKey;
    private Animator zondaAnimator;
    public PuntoPozo.Punto currentType;
    private QuestionsManager questionManager;
    private ZondaAnimationManager zondaAnimationManager;
    private PreguntasPuntosPozoManager preguntasPozo;
    private AltairScreenManager altairScreens;
    private AltairManager altair;

    private Transform _selection;

    public UnityEvent OnStartedPozoQuestion;
    public UnityEvent OnActivatePantallaFinal;
    public UnityEvent OnAnsweredHeartQuestion;
    public UnityEvent OnAnsweredFinalQuestion;
    public UnityEvent OnAnsweredExtraQuestion;

    //Contador para preguntas 
    int questionTrackingCounter = 0;
    int finalAnswerCounter = 0;

    private bool canInteract = true;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        language = LanguageManager.Instance;
        levelManager = LevelManager.Instance;
        transitionManager = TransitionManager.Instance;
        altairScreens = AltairScreenManager.Instance;
        player = PlayerManager.Instance;
        puntosManager = PuntosManager.Instance;
        questionManager = QuestionsManager.Instance;
        zondaAnimationManager = ZondaAnimationManager.Instance;
        preguntasPozo = PreguntasPuntosPozoManager.Instance;
        altair = AltairManager.Instance;
        altair.OnAltairTurnOff.AddListener(GoToScoreAfterTurnOff);
        //questionManager.OnFinishedAnsweringList.AddListener(CheckAnsweredQuestion);
        questionManager.OnAnsweredCorrectly.AddListener(CheckAnsweredQuestion);
        hudManager = HUDManager.Instance;
        currentType = PuntoPozo.Punto.None;
        //zondaAnimator = zondaAnimationManager.animator;
    }

    private void Update()
    {
        Debug.DrawRay(mainCamera.transform.position, mainCamera.transform.forward * distance, Color.red);//(transform.position, (puntoFin.transform.position - transform.position).normalized * distance, Color.red);
        if (_selection != null)
        {
            var selectionRenderer = _selection.GetComponent<Renderer>();
            bool setToVisibleMaterial = _selection.GetComponent<PuntoPozo>().makeInvisibleWhenNotPointedAt;
            if(setToVisibleMaterial)
            {
                selectionRenderer.material = defaultMaterial;
            }
            else
            {
                selectionRenderer.material = barelyVisibleMaterial;
            }
            HidePuntoPozoHud();
            _selection = null;
        }

        var ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);//(transform.position, (puntoFin.transform.position - transform.position).normalized);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, distance))
        {
            var selection = hit.transform;
            if (selection.CompareTag(selectableTag))
            {
                var selectionRenderer = selection.GetComponent<Renderer>();
                if (selectionRenderer != null)
                {
                    //Do stuff related to Punto Pozo
                    PuntoPozo p = selection.GetComponent<PuntoPozo>();
                    if (p)
                    {
                        if (!p.interactable) return;
                        selectionRenderer.material = highlightMaterial;
                        PuntoPozoLogic(p);
                    }
                }

                _selection = selection;
            }
        }
    }

    private void PuntoPozoLogic(PuntoPozo p)
    {
        if(canInteract == true)
        {
            hudManager.ShowPuntoPozoCanvas(language.GetFromCustomSheet("I_PRESS")/*"Presione "*/ + interactKey + language.GetFromCustomSheet("I_TO_INSPECT") /*" para inspeccionar"*/);
        }
        if (Input.GetKeyDown(interactKey) && canInteract)
        {
            canInteract = false;
            currentType = p.tipoDePunto;
            //if(currentType == PuntoPozo.Punto.Bodega)
            //{
            //    if (!preguntasPozo)
            //    {
            //        preguntasPozo = PreguntasPuntosPozoManager.Instance;
            //    }
            //    preguntasPozo.PlayPreguntasPreBodega();
            //    //zondaAnimator.gameObject.SetActive(true);
            //    //zondaAnimator.Play("ZondaPozo");
            //}
            //else
            //{
            OnStartedPozoQuestion?.Invoke();
            //}
            //Show question and stuff
        }
    }

    private void HidePuntoPozoHud()
    {
        hudManager.HidePuntoPozoCanvas();
    }

    private string GetName(PuntoPozo p)
    {
        switch (Convert.ToInt32(p.tipoDePunto))
        {
            case 1:
                return language.GetFromCustomSheet("PP_VALVE")/*"valvula"*/;
            case 2:
                return language.GetFromCustomSheet("PP_BRID")/*"uni�n bridada"*/;
            case 3:
                return language.GetFromCustomSheet("PP_ROSC")/*"uni�n roscada"*/;
            case 4:
                return language.GetFromCustomSheet("PP_BOD")/*"bodega"*/;
            case 5:
                return language.GetFromCustomSheet("PP_ROSC")/*"uni�n roscada"*/; //Pregunta extra, le cambiamos el nombre.
            default:
                return "not found";
        }
    }
    private void CheckAnsweredQuestion()
    {
        canInteract = true;
        if (!puntosManager)
        {
            puntosManager = PuntosManager.Instance;
        }
        //Disable all other of the same kind
        switch (currentType)
        {
            case PuntoPozo.Punto.Valvula:
                puntosManager.DisableAllValvulas();
                //if (questionTrackingCounter == 3)
                //{
                //    OnAnsweredHeartQuestion?.Invoke();
                //    StartCoroutine(waitAndFadeOut());
                //    questionTrackingCounter++;
                //}
                return;
            case PuntoPozo.Punto.Union_Bridada:
                puntosManager.DisableAllBrindadas();
                if (questionTrackingCounter == 3) //2
                {
                    finalAnswerCounter++;
                    if (finalAnswerCounter == 2)
                    {
                        OnAnsweredFinalQuestion?.Invoke();
                        player.alerts.ShowWarning(language.GetFromCustomSheet("DW_TURN_OFF")/*"Retirese y apague el equipo para finalizar."*/);
                        altair.canBeTurnedOff = true;
                    }
                    //questionTrackingCounter++;
                }
                return;
            case PuntoPozo.Punto.Union_Roscada:
                puntosManager.DisableAllRoscadas();
                return;
            case PuntoPozo.Punto.Bodega:
                //Aca contesto la primer pregunta pre pozo
                puntosManager.DisableAllBodegas();
                if (questionTrackingCounter == 0)
                {
                    if (!zondaAnimationManager)
                    {
                        zondaAnimationManager = ZondaAnimationManager.Instance;
                    }
                    zondaAnimationManager.animator.Play("ZondaPozo");
                    questionTrackingCounter++;
                }
                else if (questionTrackingCounter == 1)
                {
                    zondaAnimationManager.PauseAnimator(false);
                    questionTrackingCounter++;
                }
                else if (questionTrackingCounter == 2)
                {
                    //Mostrar cartel que invite al usuario a retirarse, activar el trigger que baja los valores y esperar que apague la alarma, despues fade in al pozo con un punto extra
                    //preguntasPozo.PlayHeartQuestion();
                    if (!preguntasPozo)
                    {
                        preguntasPozo = PreguntasPuntosPozoManager.Instance;
                    }
                    if (!player)
                    {
                        player = PlayerManager.Instance;
                    }
                    player.alerts.ShowWarning(language.GetFromCustomSheet("DW_GO_ALARM")/*"Debe volver a zona segura y apagar la alarma."*/);
                    player.detectInput = true;
                    altairScreens.optionScreen = AltairScreenManager.OptionScreen.PantallaFinal;
                    preguntasPozo.PlayBeepsAfterBodega();
                    OnActivatePantallaFinal?.Invoke();
                    questionTrackingCounter++;
                }
                return;
            case PuntoPozo.Punto.Pregunta_Extra:
                OnAnsweredExtraQuestion?.Invoke();
                //Contesto pregunta pre-corazon.
                return;
        }
    }

    private void GoToScoreAfterTurnOff()
    {
        StartCoroutine(waitAndFadeOut());
        StartCoroutine(waitAndGoToScoreScreen());
    }

    private IEnumerator waitAndFadeOut()
    {
        yield return new WaitForSeconds(2f);
        transitionManager.FadeOut();
    }

    private IEnumerator waitAndGoToScoreScreen()
    {
        yield return new WaitForSeconds(2f);
        levelManager.LoadScoreScene();
    }
}
