using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntoPozo : MonoBehaviour
{
    PlayerManager player;
    public enum Punto {None, Valvula, Union_Bridada, Union_Roscada, Bodega, Pregunta_Extra};
    public Punto tipoDePunto;
    public bool interactable;
    [SerializeField] Material unchekedMaterial;
    [SerializeField] Material pointedAtMaterial;
    [SerializeField] Material checkedMaterial;
    public bool makeInvisibleWhenNotPointedAt = true;

    private void Start()
    {
        interactable = true;
        player = PlayerManager.Instance;
    }

    public void MarkAsChecked()
    {
        if(this.isActiveAndEnabled)
        {
            interactable = false;
            GetComponent<MeshRenderer>().material = checkedMaterial;
            player.startedAnsweringPuntosPozo = true;
        }
    }
}
