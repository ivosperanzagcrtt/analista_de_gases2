using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltairGrabTrigger : MonoBehaviour
{
    LanguageManager language;
    HUDManager hud;
    PlayerManager player;
    AltairManager altair;
    [SerializeField] private KeyCode grabKey;

    //Grabbing
    [SerializeField] private GameObject altairGO;
    [SerializeField] public Transform altairHandPosition;

    private void Start()
    {
        hud = HUDManager.Instance;
        player = PlayerManager.Instance;
        altair = AltairManager.Instance;
        language = LanguageManager.Instance;

        altairHandPosition = player.altairHandPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (player.detectInput && !player.hasAltair)
        {
            hud.ShowGrabbingScreenWithText(/*"Presione '"*/language.GetFromCustomSheet("I_PRESS") + grabKey + language.GetFromCustomSheet("I_TO_GET_ALTAIR")/*"' para obtener el equipo."*/);
            //hud.ShowGrabbingScreenWithText("Presione '" + grabKey + "' para interactuar.");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        hud.HideGrabbingScreen();
    }

    private void OnTriggerStay(Collider other)
    {
        if (player.detectInput && !player.hasAltair)
        {
            if (Input.GetKeyDown(grabKey))
            {
                player.hasAltair = true;
                altair.GrabAltair();
                SetAltairParent();
            }
        }
    }

    private void SetAltairParent()
    {
        altairGO.transform.SetParent(altairHandPosition);
        altairGO.transform.localRotation = Quaternion.identity;
        altairGO.transform.localPosition = Vector3.zero;
        altairGO.transform.localScale = Vector3.one;
        hud.HideGrabbingScreen();
    }
}
