using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningSoundManager : MonoBehaviour
{
    public static WarningSoundManager Instance { get; private set; }
    private AudioSource source;
    [SerializeField] AudioClip errorSalirTrailerSound;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlayErrorSalirTrailer()
    {
        source.clip = errorSalirTrailerSound;
        source.Play();
    }
}
