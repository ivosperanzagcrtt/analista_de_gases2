using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundManager : MonoBehaviour
{
    public static PlayerSoundManager Instance { get; private set; }
    QuestionsManager questions;
    LevelManager levelManager;
    private AudioSource source;
    [SerializeField] AudioClip pasosAdentro;
    [SerializeField] AudioClip pasosAfuera;
    public bool playingPasos;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        source = GetComponent<AudioSource>();
        source.loop = true;
        source.clip = pasosAdentro;
        levelManager = LevelManager.Instance;
        questions = QuestionsManager.Instance;
        questions.OnQuestionPopup.AddListener(delegate { PlayPasos(false); });
        levelManager.OnLoadedOutsideScene.AddListener(SwitchToPasosAfuera);
    }

    public void PlayPasos(bool state)
    {
        if(state)
        {
            if (source.isPlaying) return;
            source.Play();
            playingPasos = true;
        }
        else
        {
            source.Stop();
            playingPasos = false;
        }
    }

    private void SwitchToPasosAfuera()
    {
        source.clip = pasosAfuera;
    }
}
