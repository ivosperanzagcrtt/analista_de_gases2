using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PuntosManager : MonoBehaviour
{
    PreguntasPuntosPozoManager puntosPozo;
    AltairScreenManager altairScreen;
    Zonda zonda;
    public static PuntosManager Instance { get; private set; }
    [SerializeField] List<PuntoPozo> valvulas;
    [SerializeField] List<PuntoPozo> unionesRoscadas;
    [SerializeField] List<PuntoPozo> unionesBrindadas;
    [SerializeField] List<PuntoPozo> unionesBodega;

    //Checkpoint related
    private bool checkedValvulas;
    private bool checkedBridada;
    private bool checkedRoscada;

    public UnityEvent OnCheckAllThree;
    public UnityEvent OnCheckedBoveda;

    //Punto bodega
    [SerializeField] GameObject puntoBodega;

    //Punto Corazon
    [SerializeField] GameObject puntoCorazon;

    //Punto fin del ejercicio
    [SerializeField] GameObject puntoFinEjercicio;

    //Punto extra para cubrir todas las preguntas
    [SerializeField] GameObject puntoExtra;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        zonda = Zonda.Instance;
        puntosPozo = PreguntasPuntosPozoManager.Instance;
        zonda.OnAnsweredHeartQuestion.AddListener(DoAfterPreguntaCorazon);
        zonda.OnAnsweredExtraQuestion.AddListener(delegate { StartCoroutine(waitAndActivateUltimoPunto()); }); // StartCoroutine(waitAndActivateCorazon());
        altairScreen = AltairScreenManager.Instance;
        altairScreen.OnTurnedOffFinalAlarm.AddListener(ActivarPuntoExtra);
    }

    private IEnumerator waitAndActivateUltimoPunto()
    {
        puntosPozo.shouldShowGreenAfterQuestionList = true;
        yield return new WaitForSeconds(0.05f);
        //ActivatePuntoCorazon();
        ActivarUltimoPunto();
        DesactivarPuntoExtra();
    }

    private IEnumerator waitAndActivateCorazon()
    {
        puntosPozo.shouldShowGreenAfterQuestionList = true;
        yield return new WaitForSeconds(0.05f);
        ActivatePuntoCorazon(); 
        DesactivarPuntoExtra();
    }

    private IEnumerator waitAndActivate()
    {
        yield return new WaitForSeconds(2f);
        ActivarUltimoPunto();
    }

    private void ActivarUltimoPunto()
    {
        puntoFinEjercicio.SetActive(true);
    }

    private void ActivatePuntoCorazon()
    {
        puntoCorazon.SetActive(true);
    }

    private void ActivarPuntoExtra()
    {
        puntosPozo.shouldShowGreenAfterQuestionList = true;
        puntoExtra.SetActive(true);
    }

    private void DesactivarPuntoExtra()
    {
        //puntoExtra.SetActive(false);
        puntoExtra.GetComponent<PuntoPozo>().MarkAsChecked();
    }

    private void DoAfterPreguntaCorazon()
    {
        puntoCorazon.SetActive(false);
        StartCoroutine(waitAndActivate());
    }

    public void DisableAllValvulas()
    {
        for(int i = 0; i < valvulas.Count;  i++)
        {
            var v = valvulas[i].GetComponent<PuntoPozo>();
            v.MarkAsChecked();
            checkedValvulas = true;
            CheckForAllPointsDone();
        }
    }

    public void DisableAllRoscadas()
    {
        for (int i = 0; i < unionesRoscadas.Count; i++)
        {
            var v = unionesRoscadas[i].GetComponent<PuntoPozo>();
            v.MarkAsChecked();
            checkedBridada = true;
            CheckForAllPointsDone();
        }
    }

    public void DisableAllBrindadas()
    {
        for (int i = 0; i < unionesBrindadas.Count; i++)
        {
            var v = unionesBrindadas[i].GetComponent<PuntoPozo>();
            v.MarkAsChecked();
            checkedRoscada = true;
            CheckForAllPointsDone();
        }
    }

    public void DisableAllBodegas()
    {
        for (int i = 0; i < unionesBodega.Count; i++)
        {
            var v = unionesBodega[i].GetComponent<PuntoPozo>();
            v.MarkAsChecked();
            OnCheckedBoveda?.Invoke();
        }
    }

    private void CheckForAllPointsDone()
    {
        if(checkedValvulas && checkedBridada && checkedRoscada)
        {
            OnCheckAllThree?.Invoke();
            puntoBodega.SetActive(true);
        }
    }
}
