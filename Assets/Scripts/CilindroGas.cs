using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CilindroGas : MonoBehaviour
{
    AltairManager altair;
    private LineRenderer line;
    [SerializeField] Transform entradaAltair;
    [SerializeField] Transform picoSalidaTubo;
    [SerializeField] float xOffset;
    [SerializeField] float yOffset;
    [SerializeField] float zOffset;
    [SerializeField] int positionCount;
    private void Start()
    {
        altair = AltairManager.Instance;
        line = GetComponent<LineRenderer>();
        InitLine();
        //line.enabled = false;
    }

    private void InitLine()
    {
        entradaAltair = altair.entradaGas;
        if (entradaAltair && picoSalidaTubo)
        {
            line.SetPosition(0, picoSalidaTubo.position);
            line.SetPosition(1, entradaAltair.position);
        }
        else
        {
            Debug.LogError("One of the line pos. is missing.");
        }
    }

    private void Update()
    {
        //InitLine();
        Vector3 midPos = Vector3.Lerp(picoSalidaTubo.position, entradaAltair.position, 0.5f);
        DrawQuadraticBezierCurve(picoSalidaTubo.position, new Vector3(midPos.x + xOffset, midPos.y + yOffset, midPos.z + zOffset), entradaAltair.position);
    }

    void DrawQuadraticBezierCurve(Vector3 point0, Vector3 point1, Vector3 point2)
    {
        line.positionCount = positionCount;
        float t = 0f;
        Vector3 B = new Vector3(0, 0, 0);
        for (int i = 0; i < line.positionCount; i++)
        {
            B = (1 - t) * (1 - t) * point0 + 2 * (1 - t) * t * point1 + t * t * point2;
            line.SetPosition(i, B);
            t += (1 / (float)line.positionCount);
        }
    }
}
