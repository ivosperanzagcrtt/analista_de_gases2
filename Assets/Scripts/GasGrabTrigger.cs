using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasGrabTrigger : MonoBehaviour
{
    LanguageManager language;
    HUDManager hud;
    PlayerManager player;
    //AltairManager altair;
    [SerializeField] private KeyCode grabKey;

    //Grabbing
    [SerializeField] private GameObject gasTubeGO;
    [SerializeField] public Transform gasHandPosition;

    private void Start()
    {
        hud = HUDManager.Instance;
        player = PlayerManager.Instance;
        language = LanguageManager.Instance;
        gasHandPosition = player.handGasPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (player.hasGas) return;
        if (player.detectInput)
        {
            hud.ShowGrabbingScreenWithText(/*"Presione '"*/language.GetFromCustomSheet("I_PRESS") + grabKey + language.GetFromCustomSheet("I_TO_GET_GAS")/*"' para obtener el gas."*/);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (player.hasGas) return;
        hud.HideGrabbingScreen();
    }

    private void OnTriggerStay(Collider other)
    {
        if (player.detectInput)
        {
            if (Input.GetKeyDown(grabKey))
            {
                player.hasGas = true;
                SetGasTubeParent();
            }
        }
    }

    private void SetGasTubeParent()
    {
        gasTubeGO.transform.SetParent(gasHandPosition);
        gasTubeGO.transform.localRotation = Quaternion.identity;
        gasTubeGO.transform.localPosition = Vector3.zero;
        gasTubeGO.transform.localScale = Vector3.one;
        hud.HideGrabbingScreen();
    }
}
