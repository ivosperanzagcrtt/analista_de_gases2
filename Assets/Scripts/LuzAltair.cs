using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzAltair : MonoBehaviour
{
    AltairManager altair;
    [SerializeField] GameObject luz1;
    [SerializeField] GameObject luz2;
    private MeshRenderer renderer1;
    private MeshRenderer renderer2;
    [SerializeField] Material off;
    [SerializeField] Material verde;
    [SerializeField] Material roja;

    private bool blinkGreen = false;
    private bool blinkingRed = false;

    Coroutine greenBlinking;
    private void Start()
    {
        renderer1 = luz1.GetComponent<MeshRenderer>();
        renderer2 = luz2.GetComponent<MeshRenderer>();
        altair = AltairManager.Instance;
    }

    public void Apagar(bool proceedWithGreenBlink)
    {
        StopAllCoroutines();
        blinkingRed = false;
        if (proceedWithGreenBlink)
        {
            BlinkGreen(true);
        }
        else
        {
            if(greenBlinking != null)
            {
                StopCoroutine(greenBlinking);
            }
            renderer1.material = off;
            renderer2.material = off;
        }
    }

    public void PrenderVerde()
    {
        if (blinkingRed) return;
        renderer1.material = verde;
        renderer2.material = verde;
    }

    public void PrenderRoja()
    {
        StopAllCoroutines();
        if (greenBlinking != null)
        {
            StopCoroutine(greenBlinking);
        }

        blinkingRed = true;
        renderer1.material = roja;
        renderer2.material = roja;
    }

    public void BlinkGreen(bool state)
    {
        StopAllCoroutines();
        if(!altair.isOn)
        {
            Apagar(false);
        }
        else
        {
            if (blinkingRed) return;
            if (state)
            {
                blinkGreen = true;
                greenBlinking = StartCoroutine(BlinkGreen());
            }
            else
            {
                blinkGreen = false;
                StopAllCoroutines();
                Apagar(false);
            }
        }
    }

    private IEnumerator BlinkGreen()
    {
        if (blinkGreen)
        {
            renderer1.material = verde;
            renderer2.material = verde;
            yield return new WaitForSeconds(0.5f);
            renderer1.material = off;
            renderer2.material = off;
            //yield return new WaitForSeconds(0.5f);
            //renderer1.material = verde;
            //renderer2.material = verde;
            //yield return new WaitForSeconds(0.5f);
            //renderer1.material = off;
            //renderer2.material = off;
            //yield return new WaitForSeconds(0.5f);
            //renderer1.material = verde;
            //renderer2.material = verde;
            //yield return new WaitForSeconds(0.5f);
            //renderer1.material = off;
            //renderer2.material = off;
            yield return new WaitForSeconds(10f);
            greenBlinking = StartCoroutine(BlinkGreen());
        }
    }

}
