using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltairButtonsCursorChange : MonoBehaviour
{
    CursorManager cursor;
    QuestionsManager questions;

    private void Start()
    {
        cursor = CursorManager.Instance;
        questions = QuestionsManager.Instance;
        questions.OnQuestionPopup.AddListener(delegate { cursor.UseCustomCursor(false); });
    }

    void OnMouseEnter()
    {
        if (questions.isOpen) return;
        cursor.UseCustomCursor(true);
    }

    void OnMouseExit()
    {
        cursor.UseCustomCursor(false);
    }
}
