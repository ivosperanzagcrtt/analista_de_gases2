using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Polyglot;

public class MainMenuManager : MonoBehaviour
{
    LevelManager levelManager;
    MenuMusicManager menuMusicManager;
    LanguageManager languageManager;
    [SerializeField] private Button nextButton;
    [SerializeField] private Button backButton;
    [SerializeField] List<CanvasGroup> screens;
    private int currentSlide = 0;

    //Texts
    [SerializeField] TextMeshProUGUI buttonText;
    private void Start()
    {
        levelManager = LevelManager.Instance;
        menuMusicManager = MenuMusicManager.Instance;
        languageManager = LanguageManager.Instance;
        languageManager.OnLanguageChange.AddListener(ChangeButtonText);
        backButton.onClick.AddListener(Back);
        nextButton.onClick.AddListener(ShowSlide);
        ChangeButtonText();
    }

    private void Back()
    {
        if (currentSlide > 0)
        {
            currentSlide--;
            ChangeButtonText();
            ShowCanvas(screens[currentSlide + 1], false);
            ShowCanvas(screens[currentSlide], true);
            if (currentSlide == 0)
            {
                ShowBackButton(false);
                return;
            }
        }
    }

    private void ShowBackButton(bool state)
    {
        if(state)
        {
            backButton.gameObject.SetActive(true);
        }
        else
        {
            backButton.gameObject.SetActive(false);
        }
    }

    private void ShowSlide()
    {
        if(currentSlide < screens.Count - 1)
        {
            currentSlide++;
            ChangeButtonText();
            ShowCanvas(screens[currentSlide - 1], false);
            ShowCanvas(screens[currentSlide], true);
        }
        else
        {
            StartCoroutine(waitAndLoadLevel());
        }

        if (currentSlide > 0)
        {
            ShowBackButton(true);
        }

    }

    private IEnumerator waitAndLoadLevel()
    {
        menuMusicManager.PlayMenuMusic(false);
        backButton.interactable = false;
        nextButton.interactable = false;
        menuMusicManager.PlayTransition();
        yield return new WaitForSeconds(1.5f);
        levelManager.LoadInsideTrailerScene();
    }

    private void ChangeButtonText()
    {
        switch(currentSlide)
        {
            case 0:
                buttonText.text = languageManager.GetFromCustomSheet("MM_Intro");//"INTRO";
                break;
            case 1:
                buttonText.text = languageManager.GetFromCustomSheet("MM_Next"); //"SIGUIENTE";
                break;
            case 3:
                buttonText.text = languageManager.GetFromCustomSheet("MM_Start"); //"COMENZAR";
                break;
        }
    }

    private void ShowCanvas(CanvasGroup c, bool state)
    {
        if(state)
        {
            c.alpha = 1f;
        }
        else
        {
            c.alpha = 0f;
        }
    }

}
