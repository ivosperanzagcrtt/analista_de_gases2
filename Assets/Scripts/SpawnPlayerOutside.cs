using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayerOutside : MonoBehaviour
{
    PlayerManager player;
    AltairScreenManager altairScreen;
    [SerializeField] Transform spawnPoint;
    [SerializeField] Transform pantallaFinalPositionPlayer;

    private void Start()
    {
        player = PlayerManager.Instance;
        altairScreen = AltairScreenManager.Instance;
        //altairScreen.OnTurnedOffFinalAlarm.AddListener(SpawnAtPantallaFinalPosition);
        SpawnAndRotate();
    }

    //private void SpawnAtPantallaFinalPosition()
    //{
    //    if (player && pantallaFinalPositionPlayer)
    //    {
    //        player.playerController.gameObject.transform.position = pantallaFinalPositionPlayer.position;
    //        player.playerController.gameObject.transform.rotation = pantallaFinalPositionPlayer.rotation;
    //    }
    //    else
    //    {
    //        Debug.LogError("No player or transform found.");
    //    }
    //}    

    private void SpawnAndRotate()
    {
        if(player && spawnPoint)
        {
            player.gameObject.transform.position = spawnPoint.position;
            player.gameObject.transform.rotation = spawnPoint.rotation;
            player.detectInput = true;
        }
        else
        {
            Debug.LogError("No player or transform found.");
        }
    }

}
