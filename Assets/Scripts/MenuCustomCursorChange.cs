using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuCustomCursorChange : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    CursorManager cursor;
    Button button;

    private void Start()
    {
        cursor = CursorManager.Instance;
        button = GetComponent<Button>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        cursor.UseCustomCursor(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        cursor.UseCustomCursor(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!button)
        {
            Debug.Log("No button found.");
            return;
        }

        if (!button.gameObject.activeSelf)
        {
            cursor.UseCustomCursor(false);
        }
    }
}
