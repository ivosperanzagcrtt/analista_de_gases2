using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class QuestionTrigger : MonoBehaviour
{
    PlayerManager player;
    Zonda zonda;
    GasChartManager charts;
    QuestionsManager questionManager;
    private bool interactable = false;
    [SerializeField] private List<Question> questions;
    public int questionnaireNumber;

    //
    private bool backToNormalValues = true;
    private int correctAnswers = 0;
    private void Start()
    {
        player = PlayerManager.Instance;
        zonda = Zonda.Instance;
        questionManager = QuestionsManager.Instance;
        charts = GasChartManager.Instance;

        questionManager.OnAnsweredCorrectly.AddListener(BackToNormalValuesCheck);

        if (questionnaireNumber == 2)
        {
            zonda.OnActivatePantallaFinal.AddListener(delegate
            {
                interactable = true;
            });
        }
        else
        {
            StartCoroutine(WaitAndMakeInteractable());
        }
    }

    private void BackToNormalValuesCheck()
    {
        correctAnswers++;
        if (backToNormalValues && correctAnswers == 2)
        {
            charts.Mitad_CaminoAlPozo(false);
            charts.AllGreenAndNormal(true);
            backToNormalValues = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!interactable) return;
        //if (questions.Count == 0) return;
        if(questionnaireNumber == 2)
        {
            interactable = false;
            CheckQuestionnaire();
        }
        else
        {
            StartCoroutine(WaitAndDisplayQuestions(1f));
        }
    }

    IEnumerator WaitAndMakeInteractable()
    {
        yield return new WaitForSeconds(2f);
        interactable = true;
    }
    IEnumerator WaitAndDisplayQuestions(float time)
    {
        interactable = false;
        CheckQuestionnaire();
        if (questions.Count == 0)
        {
            yield break;
        }
        yield return new WaitForSeconds(time);
        questionManager.PlayList(questions);
    }

    private void CheckQuestionnaire()
    {
        switch (questionnaireNumber)
        {
            case 1:
                charts.Mitad_CaminoAlPozo(true);
                break;
            case 2:
                interactable = false;
                //charts.Mitad_CaminoAlPozo(false);
                charts.B_MitadPozoVolviendo(true);
                //player.SetInputState(false);//
                //player.playerController.SetCursorVisible(true);
                //player.playerController.SetCanMove(false);//
                break;
            default:
                Debug.Log("No questionnaire found");
                break;
        }
    }
}
