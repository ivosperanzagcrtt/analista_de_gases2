using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFarClipping : MonoBehaviour
{
    AdjustFarClipPlane farClip;

    private void Start()
    {
        farClip = AdjustFarClipPlane.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        farClip.ChangeToStartingFarClipPlane();
    }

    private void OnTriggerExit(Collider other)
    {
        farClip.ChangeToOutsideFarClipPlane();
    }
}
