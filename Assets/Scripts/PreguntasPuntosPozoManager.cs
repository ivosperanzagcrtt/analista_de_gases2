using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreguntasPuntosPozoManager : MonoBehaviour
{
    public static PreguntasPuntosPozoManager Instance { get; private set; }
    QuestionsManager questionManager;
    GasChartManager chartManager;
    AltairSoundLightManager altairLights;
    AltairScreenManager altairScreen;
    Zonda zonda;

    [SerializeField] List<Question> A;
    [SerializeField] List<Question> B;
    [SerializeField] List<Question> C;
    [SerializeField] List<Question> D;
    [SerializeField] List<Question> E_PreBodega;
    [SerializeField] List<Question> F_Adentro_Bodega;
    [SerializeField] List<Question> G_Corazon;
    [SerializeField] List<Question> H_Final1;

    List<List<Question>> listaPreguntas = new List<List<Question>>();

    //Bools
    private bool q_A = false;
    private bool q_B = false;
    private bool q_C = false;
    private bool q_D = false;

    private bool canPlayCorazonQuestion = false;
    private bool canPlayLastQuestion = false;
    public bool shouldShowGreenAfterQuestionList = false;

    private List<int> validChoices = new List<int>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        questionManager = QuestionsManager.Instance;
        chartManager = GasChartManager.Instance;
        altairLights = AltairSoundLightManager.Instance;
        zonda = Zonda.Instance;
        altairScreen = AltairScreenManager.Instance;
        altairScreen.OnTurnedOffFinalAlarm.AddListener(delegate { /*canPlayCorazonQuestion = true;*/ altairLights.PlayAlarmaAlta(false); });
        zonda.OnAnsweredExtraQuestion.AddListener(delegate { canPlayLastQuestion = true; });
        zonda.OnStartedPozoQuestion.AddListener(StartRandomQuestion);
        //zonda.OnAnsweredHeartQuestion.AddListener(DoAfterHeartQuestion);
        questionManager.OnFinishedAnsweringList.AddListener(delegate { altairLights.PlayHermeticTestBeeps(false); altairLights.PlayAlarmaAlta(false); GoBackToNormalChart(); });
        AddToLists();
    }

    private void GoBackToNormalChart()
    {
        if(shouldShowGreenAfterQuestionList)
        {
            chartManager.AllGreenAndNormal(true);
        }
    }

    private void DoAfterHeartQuestion()
    {
        chartManager.ShowHeart(false);
    }

    private void AddToLists()
    {
        listaPreguntas.Add(A);
        listaPreguntas.Add(B);
        listaPreguntas.Add(C);
        listaPreguntas.Add(D);
        validChoices.Add(0);
        validChoices.Add(1);
        validChoices.Add(2);
        validChoices.Add(3);
    }

    private void StartRandomQuestion()
    {
        switch (zonda.currentType)
        {
            case PuntoPozo.Punto.Valvula:
                if(!canPlayCorazonQuestion)
                {
                    PlayRandomQuestion();
                }
                else
                {
                    //PlayHeartQuestion();
                }
                return;
            case PuntoPozo.Punto.Union_Bridada:
                if(!canPlayLastQuestion)
                {
                    PlayRandomQuestion();
                }
                else
                {
                    PlayFinal1();
                }
                return;
            case PuntoPozo.Punto.Union_Roscada:
                PlayRandomQuestion();
                return;
            case PuntoPozo.Punto.Bodega:
                PlayPreguntasPreBodega();
                return;
            case PuntoPozo.Punto.Pregunta_Extra:
                PlayRandomQuestion();
                return;
        }
    }

    private void PlayRandomQuestion()
    {
        int r = ValidRandomNumber();
        RemoveLastRandomFromList(r);
        if (r >= 0 && r < listaPreguntas.Count)
        {
            CheckQuestionToMatchAltairScreen(r);
            questionManager.PlayList(listaPreguntas[r]);
        }
        else
        {
            Debug.Log("R: " + r + " - No more questions for these points.");
        }
    }

    private int ValidRandomNumber()
    {
        int r = validChoices[Random.Range(0, validChoices.Count)];
        return r;
    }

    private void RemoveLastRandomFromList(int number)
    {
        for (int i = 0; i < validChoices.Count; i++)
        {
            if (number == validChoices[i])
            {
                validChoices.RemoveAt(i);
            }
        }
    }

    private void CheckQuestionToMatchAltairScreen(int index)
    {
        shouldShowGreenAfterQuestionList = true;
        ////This needs refactoring
        //if (index == 0 && q_A ||
        //    index == 1 && q_B ||
        //    index == 2 && q_C ||
        //    index == 3 && q_D)
        //{
        //    CheckQuestionToMatchAltairScreen(index);
        //}


        //chartManager.CleanScreens();
        chartManager.Question_A(false);
        chartManager.Question_B(false);
        chartManager.Question_C(false);
        chartManager.Question_D(false);
        chartManager.Question_E(false);

        switch (index)
        {
            case 0:
                //A
                altairScreen.Question_A_B_D();
                altairLights.PlayAlarmaAlta(true);
                chartManager.Question_A(true);
                q_A = true;
                return;
            case 1:
                //B
                altairScreen.Question_B();
                altairScreen.Question_A_B_D();
                altairLights.PlayAlarmaAlta(true);
                chartManager.Question_B(true);
                q_B = true;
                return;
            case 2:
                //C
                chartManager.Question_C(true);
                q_C = true;
                return;
            case 3:
                //D
                altairScreen.Question_A_B_D();
                altairLights.PlayAlarmaAlta(true);
                chartManager.Question_D(true);
                q_D = true;
                return;
        }
    }

    public void PlayPreguntasPreBodega()
    {
        questionManager.PlayList(E_PreBodega);
    }

    public void PlayPreguntasAdentroBodega()
    {
        questionManager.PlayList(F_Adentro_Bodega);
        altairScreen.Question_A_B_D();
        altairLights.PlayAlarmaAlta(true);
        chartManager.Question_E(true);
    }

    public void PlayBeepsAfterBodega()
    {
        shouldShowGreenAfterQuestionList = false;
        StartCoroutine(waitAndPlayBeeps());
    }

    IEnumerator waitAndPlayBeeps()
    {
        yield return new WaitForSeconds(0.5f);
        altairLights.PlayAlarmaAlta(true);
    }

    //public void PlayHeartQuestion()
    //{
    //    shouldShowGreenAfterQuestionList = false;
    //    chartManager.ShowHeart(true);
    //    questionManager.PlayList(G_Corazon);
    //    altairScreen.Question_B();
    //    chartManager.Question_F(true);
    //}

    public void PlayFinal1()
    {
        chartManager.ShowHeart(false);
        chartManager.Question_F(true);
        questionManager.PlayList(H_Final1);
    }
}
