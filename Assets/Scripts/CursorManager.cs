using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    public static CursorManager Instance { get; private set; }
    [SerializeField] private Texture2D customCursor;
    [SerializeField] private Texture2D customCursor_WebGL;
    private AudioSource clickSource;
    private bool isShowing = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        clickSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (!isShowing) return;
        if(Input.GetMouseButtonDown(0))
        {
            clickSource.Play();
        }
    }

    public void UseCustomCursor(bool state)
    {
        if (state)
        {
            if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
            {
                Cursor.SetCursor(customCursor, Vector2.zero, CursorMode.Auto);
            }
            else if(Application.platform == RuntimePlatform.WebGLPlayer)
            {
                Cursor.SetCursor(customCursor_WebGL, Vector2.zero, CursorMode.Auto);
            }

            isShowing = true;

        }
        else
        {
            isShowing = false;
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }
}
