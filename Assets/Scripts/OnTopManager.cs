using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTopManager : MonoBehaviour
{
    AltairManager altair;
    [SerializeField] Camera onTopCamera;
    [SerializeField] LayerMask layerMask;

    private void Start()
    {
        altair = AltairManager.Instance;
        altair.OnGrabbedFromChargeStation.AddListener(ChangeCullingMask);
    }

    private void ChangeCullingMask()
    {
        onTopCamera.cullingMask = layerMask;
    }
}
