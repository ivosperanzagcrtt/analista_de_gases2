using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class AdjustFarClipPlane : MonoBehaviour
{
    public static AdjustFarClipPlane Instance { get; private set; }
    LevelManager levelManager;
    CinemachineVirtualCamera camera;

    //Starting
    [SerializeField] private float startingFarPlane;

    //Outside
    [SerializeField] private float outsideFarPlane;

    //Outside
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        camera = GetComponent<CinemachineVirtualCamera>();
        ChangeFarClipPlane(startingFarPlane);
        levelManager = LevelManager.Instance;
        levelManager.OnLoadedOutsideScene.AddListener(delegate { ChangeFarClipPlane(outsideFarPlane); });
    }

    private void ChangeFarClipPlane(float value)
    {
        camera.m_Lens.FarClipPlane = value;
    }

    public void ChangeToStartingFarClipPlane()
    {
        camera.m_Lens.FarClipPlane = startingFarPlane;
    }

    public void ChangeToOutsideFarClipPlane()
    {
        camera.m_Lens.FarClipPlane = outsideFarPlane;
    }
}
