using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GasChartManager : MonoBehaviour
{
    public static GasChartManager Instance { get; private set; }

    [SerializeField] AltairChartSection topLeft;
    [SerializeField] AltairChartSection topRight;
    [SerializeField] AltairChartSection bottomLeft;
    [SerializeField] AltairChartSection bottomRight;
    [SerializeField] Image heartIcon;
    private bool showingHeart = false;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ShowHeart(bool state)
    {
        if (state)
        {
            showingHeart = true;
            StartCoroutine(BlinkHeart());
        }
        else
        {
            showingHeart = false;
        }
    }

    private IEnumerator BlinkHeart()
    {
        heartIcon.enabled = true;
        yield return new WaitForSeconds(0.5f);
        heartIcon.enabled = false;
        yield return new WaitForSeconds(0.5f);
        if (showingHeart)
        {
            StartCoroutine(BlinkHeart());
        }
        else
        {
            ShowHeart(false);
            heartIcon.enabled = false;
        }
    }

    public void ShowAll(bool state)
    {
        if (state)
        {
            topLeft.Show(true);
            topRight.Show(true);
            bottomLeft.Show(true);
            bottomRight.Show(true);
        }
        else
        {
            topLeft.Show(false);
            topRight.Show(false);
            bottomLeft.Show(false);
            bottomRight.Show(false);
        }
    }
    public void ShowTopLeft(bool state)
    {
        if (state)
        {
            topLeft.Show(true);
        }
        else
        {
            topLeft.Show(false);
        }
    }
    public void ShowTopRight(bool state)
    {
        if (state)
        {
            topRight.Show(true);
        }
        else
        {
            topRight.Show(false);
        }
    }
    public void ShowBottomLeft(bool state)
    {
        if (state)
        {
            bottomLeft.Show(true);
        }
        else
        {
            bottomLeft.Show(false);
        }
    }
    public void ShowBottomRight(bool state)
    {
        if (state)
        {
            bottomRight.Show(true);
        }
        else
        {
            bottomRight.Show(false);
        }
    }

    public void CleanScreens()
    {
        ShowAll(false);
        //TOP LEFT
        topLeft.ShowValueText(false);
        topLeft.SetChartColor(0);
        topLeft.SetValue(0);

        //TOP RIGHT
        topRight.ShowValueText(false);
        topRight.SetChartColor(0);
        topRight.SetValue(0);

        //BOTTOM LEFT
        bottomLeft.ShowValueText(false);
        bottomLeft.SetChartColor(0);
        bottomLeft.SetValue(0);

        //BOTTOM RIGHT
        bottomRight.ShowValueText(false);
        bottomRight.SetChartColor(0);
        bottomRight.SetValue(0);
    }

    public void AlarmaBaja(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.SetChartColor(2);
            topLeft.SetValue(10);

            //TOP RIGHT
            topRight.SetChartColor(3);
            topRight.SetValue(19.5f);

            //BOTTOM LEFT
            bottomLeft.SetChartColor(2);
            bottomLeft.SetValue(25);

            //BOTTOM RIGHT
            bottomRight.SetChartColor(2);
            bottomRight.SetValue(10);
        }
        else
        {
            ShowAll(false);
        }
    }
    public void AlarmaAlta(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.SetChartColor(3);
            topLeft.SetValue(20);

            //TOP RIGHT
            topRight.SetChartColor(2);
            topRight.SetValue(23.0f);

            //BOTTOM LEFT
            bottomLeft.SetChartColor(3);
            bottomLeft.SetValue(100);

            //BOTTOM RIGHT
            bottomRight.SetChartColor(3);
            bottomRight.SetValue(15);

        }
        else
        {
            ShowAll(false);
        }
    }
    public void AlarmaStel(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.Show(false);

            //TOP RIGHT
            topRight.Show(false);

            //BOTTOM LEFT
            bottomLeft.SetChartColor(3);
            bottomLeft.SetValue(100);

            //BOTTOM RIGHT
            bottomRight.SetChartColor(3);
            bottomRight.SetValue(15);
        }
        else
        {
            ShowAll(false);
        }
    }
    public void AlarmaTWA(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.Show(false);

            //TOP RIGHT
            topRight.Show(false);

            //BOTTOM LEFT
            bottomLeft.SetChartColor(3);
            bottomLeft.SetValue(25);

            //BOTTOM RIGHT
            bottomRight.SetChartColor(3);
            bottomRight.SetValue(10);
        }
        else
        {
            ShowAll(false);
        }
    }
    public void BotellaCalibracion(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.SetChartColor(0);
            topLeft.SetValue(58);

            //TOP RIGHT
            topRight.SetChartColor(0);
            topRight.SetValue(15.0f);

            //BOTTOM LEFT
            bottomLeft.SetChartColor(0);
            bottomLeft.SetValue(60);

            //BOTTOM RIGHT
            bottomRight.SetChartColor(0);
            bottomRight.SetValue(20);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void AjusteAireLimpio(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.SetChartColor(0);
            topLeft.SetValue(0);

            //TOP RIGHT
            topRight.SetChartColor(0);
            topRight.SetValue(20.8f);

            //BOTTOM LEFT
            bottomLeft.SetChartColor(0);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.SetChartColor(0);
            bottomRight.SetValue(0);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void AireLimpioSatisfactorio(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            //topLeft.SetChartColor(0);
            //topLeft.SetValue(0);
            topLeft.ShowValueText(false);
            topLeft.ShowGreenCheckmark(true);

            //TOP RIGHT
            //topRight.SetChartColor(0);
            //topRight.SetValue(20.8f);
            topRight.ShowValueText(false);
            topRight.ShowGreenCheckmark(true);

            //BOTTOM LEFT
            //bottomLeft.SetChartColor(0);
            //bottomLeft.SetValue(0);
            bottomLeft.ShowValueText(false);
            bottomLeft.ShowGreenCheckmark(true);

            //BOTTOM RIGHT
            //bottomRight.SetChartColor(0);
            //bottomRight.SetValue(0);
            bottomRight.ShowValueText(false);
            bottomRight.ShowGreenCheckmark(true);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void AllGreenAndNormal(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(1);
            topLeft.SetValue(0);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(1);
            topRight.SetValue(20.8f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(1);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(1);
            bottomRight.SetValue(0);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void BeforeBumpTest(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(0);
            topLeft.SetValue(58);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(0);
            topRight.SetValue(15.0f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(0);
            bottomLeft.SetValue(60);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(0);
            bottomRight.SetValue(20);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void BumpTest_1(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(0);
            topLeft.SetValue(0);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(0);
            topRight.SetValue(20.8f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(0);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(0);
            bottomRight.SetValue(0);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void BumpTest_Passed(bool state)
    {
        if (state)
        {
            ShowAll(true);
            topLeft.ShowValueText(false);
            topLeft.ShowGreenCheckmark(true);

            topRight.ShowValueText(false);
            topRight.ShowGreenCheckmark(true);

            bottomLeft.ShowValueText(false);
            bottomLeft.ShowGreenCheckmark(true);

            bottomRight.ShowValueText(false);
            bottomRight.ShowGreenCheckmark(true);
        }
        else
        {
            ShowAll(false);
        }
    }

    //Outside

    public void Mitad_CaminoAlPozo(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(1);
            topLeft.SetValue(0);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(1);
            topRight.SetValue(20.8f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(1);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(1);
            bottomRight.SetValue(8);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void Question_A(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(1);
            topLeft.SetValue(0);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(1);
            topRight.SetValue(20.8f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(1);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(3);
            bottomRight.SetValue(32);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void Question_B(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(3);
            topLeft.SetValue(35);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(1);
            topRight.SetValue(19.9f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(1);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(1);
            bottomRight.SetValue(0);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void Question_C(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(1);
            topLeft.SetValue(6);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(1);
            topRight.SetValue(20.8f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(1);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(1);
            bottomRight.SetValue(0);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void Question_D(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(3);
            topLeft.SetValueAsString("XXX");

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(1);
            topRight.SetValue(20.8f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(1);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(1);
            bottomRight.SetValue(0);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void Question_E(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(3);
            topLeft.SetValue(80);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(3);
            topRight.SetValue(15.3f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(1);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(3);
            bottomRight.SetValue(22);
        }
        else
        {
            ShowAll(false);
        }
    }

    public void Question_F(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(1);
            topLeft.SetValue(0);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(1);
            topRight.SetValue(20.8f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(1);
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(1);
            bottomRight.SetValue(0);

        }
        else
        {
            ShowAll(false);
        }
    }

    public void B_MitadPozoVolviendo(bool state)
    {
        if (state)
        {
            ShowAll(true);

            //TOP LEFT
            topLeft.ShowValueText(true);
            topLeft.SetChartColor(3); //Pre: 1
            topLeft.SetValue(0);

            //TOP RIGHT
            topRight.ShowValueText(true);
            topRight.SetChartColor(3); //Pre: 1
            topRight.SetValue(20.8f);

            //BOTTOM LEFT
            bottomLeft.ShowValueText(true);
            bottomLeft.SetChartColor(1); //Pre: 1
            bottomLeft.SetValue(0);

            //BOTTOM RIGHT
            bottomRight.ShowValueText(true);
            bottomRight.SetChartColor(3);
            bottomRight.SetValue(0);
        }
        else
        {
            ShowAll(false);
        }
    }
}
