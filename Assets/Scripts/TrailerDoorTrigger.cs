using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailerDoorTrigger : MonoBehaviour
{
    LanguageManager language;
    ScoreManager score;
    QuestionLoggerManager logger;
    AltairScreenManager altairScreen;
    LevelManager levelManager;
    HUDManager hud;
    PlayerManager player;
    WarningSoundManager warningSound;
    [SerializeField] private KeyCode leaveKey;
    private bool writeToLog = true;

    private void Start()
    {
        logger = QuestionLoggerManager.Instance;
        altairScreen = AltairScreenManager.Instance;
        hud = HUDManager.Instance;
        player = PlayerManager.Instance;
        levelManager = LevelManager.Instance;
        score = ScoreManager.Instance;
        warningSound = WarningSoundManager.Instance;
        language = LanguageManager.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        hud.ShowGoOutsideCanvas(/*"Presione '"*/language.GetFromCustomSheet("I_PRESS") + leaveKey + language.GetFromCustomSheet("I_TO_GO_OUT")/*"' para ir afuera."*/);
    }

    private void OnTriggerExit(Collider other)
    {
        hud.HideGoOutsideCanvas();
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(leaveKey))
        {
            if (player.canGoOutside)
            {
                altairScreen.optionScreen = AltairScreenManager.OptionScreen.None;
                hud.HideGoOutsideCanvas();
                player.hasZonda = true;
                player.lHand = PlayerManager.LeftHandState.Zonda;
                player.CheckLeftHandState();
                player.detectInput = false;
                levelManager.LoadOutsideScene();
                player.SetLeftHandVisible(true);
                logger.AddToLog(/*"<color=#2F4F4F>El usuario sali� del trailer.</color>"*/language.GetFromCustomSheet("L_EXITED"));
                logger.AddToLog(" ");
            }
            else
            {
                warningSound.PlayErrorSalirTrailer();
                player.alerts.ShowWarning(/*"El equipo no se encuentra con las verificaciones m�nimas para asegurar su correcto funcionamiento."*/language.GetFromCustomSheet("DW_REQUIREMENTS"));
                if (writeToLog)
                {
                    logger.AddToIncorrectlyAnsweredLog(" ");
                    logger.AddToIncorrectlyAnsweredLog(/*"- Se intent� salir del trailer sin tener las verificaciones m�nimas -"*/language.GetFromCustomSheet("L_TRIED_OUT"));
                    logger.AddToIncorrectlyAnsweredLog(" ");
                    writeToLog = false;
                }
                score.RestarPuntaje(5);
            }
        }
    }
}
