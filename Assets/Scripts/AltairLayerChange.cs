using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltairLayerChange : MonoBehaviour
{
    AltairManager altair;

    //[SerializeField] Shader unlit;
    //[SerializeField] Shader standard;

    //[SerializeField] Material mat1;
    //[SerializeField] Material mat2;
    //[SerializeField] Material mat3;

    private void Start()
    {
        altair = AltairManager.Instance;
        altair.OnGrabbedFromChargeStation.AddListener(delegate { ChangeLayer(6); });
        ChangeLayer(0);
    }

    private void ChangeLayer(int layer)
    {
        gameObject.layer = layer;
        foreach (Transform child in gameObject.transform)
        {
            child.gameObject.layer = layer;
        }
    }

    //private void ChangeToStandard()
    //{
    //    mat1.shader = standard;
    //    mat2.shader = standard;
    //    mat3.shader = standard;
    //}

    //private void SwapShaders()
    //{
    //    mat1.shader = unlit;
    //    mat2.shader = unlit;
    //    mat3.shader = unlit;
    //}
}
