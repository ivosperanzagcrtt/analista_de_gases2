using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance { get; private set; }
    private int puntaje = 100;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public int GetPuntaje()
    {
        return puntaje;
    }

    public void SumarPuntaje(int puntos)
    {
        puntaje += puntos;
    }

    public void RestarPuntaje(int puntos)
    {
        puntaje -= puntos;
    }
}
