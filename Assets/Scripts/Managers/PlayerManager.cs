using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private LanguageManager lang;
    public static PlayerManager Instance { get; private set; }
    private PlayerSoundManager playerSound;
    public WarningMessages alerts;
    private HUDManager hUDManager;
    private LevelManager levelManager;
    private QuestionsManager questionManager;
    [SerializeField] public Camera mainCamera;
    [SerializeField] public FirstPersonController playerController;

    public bool detectInput = true;
    public bool showingRightHand = false;
    private bool showingLeftHand = false;
    //Right hand
    [SerializeField] GameObject rightHand;
    public enum LeftHandState {None, HermeticTest, HoldGas, Zonda};
    public LeftHandState lHand = LeftHandState.HermeticTest;

    //Left hand
    [SerializeField] GameObject leftHand;
    [SerializeField] GameObject pruebaHermeticidad;
    [SerializeField] GameObject gasHolding;
    [SerializeField] GameObject zonda;
    //Inventory
    public bool hasAltair;
    public Transform altairHandPosition;

    public bool hasGas;
    public Transform handGasPosition;

    public bool hasZonda;

    //Belt
    AltairManager altair;
    public Transform toolBelt;    

    //General
    public bool mouseVisible = false;
    public bool canGoOutside = false;
    public bool isOutside = false;
    public bool startedAnsweringPuntosPozo = false;
    public int listsAnswered = 0;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        DontDestroyOnLoad(this);
        lang = LanguageManager.Instance;
        playerSound = PlayerSoundManager.Instance;
        alerts = WarningMessages.Instance;
        altair = AltairManager.Instance;
        playerController = FirstPersonController.Instance;
        hUDManager = HUDManager.Instance;
        levelManager = LevelManager.Instance;
        questionManager = QuestionsManager.Instance;
        questionManager.OnFinishedAnsweringList.AddListener(CheckForOutsideAlerts);
        levelManager.OnLoadedOutsideScene.AddListener(delegate { isOutside = true; });
        playerController.SetCursorVisible(mouseVisible);
        HideRightHand();
        hUDManager.ShowInstructions(true);
    }
    private void Update()
    {
        CheckInput();
    }
    private void CheckForOutsideAlerts()
    {
        if (isOutside && startedAnsweringPuntosPozo) //Add restriction for final ones
        {
            listsAnswered++;
            if (listsAnswered < 4) //7 == Nuevo final (sin corazon)
            {
                alerts.ShowWarning(lang.GetFromCustomSheet("DW_GOOD")/*"Bien, contin�e midiendo."*/);
            }
            else if(listsAnswered == 6)
            {
                alerts.ShowWarning(lang.GetFromCustomSheet("DW_LAST_POINT")/*"Busque el �ltimo punto de detecci�n."*/);
            }
        }
    }

    public void SetRightHandVisible(bool state)
    {
        if(state)
        {
            altair.MoveToHand();
            ShowRightHand();
        }
        else
        {
            altair.MoveToBelt();
            HideRightHand();
        }
    }
    private void ShowRightHand()
    {
        rightHand.SetActive(true);
        //playerController.SetCanMove(false);
        //playerController.SetCursorVisible(true);
    }
    private void HideRightHand()
    {
        rightHand.SetActive(false);
        playerController.SetCanMove(true);
        //playerController.SetCursorVisible(false);
    }
    public void SetLeftHandVisible(bool state)
    {
        if (state)
        {
            ShowLeftHand();
            showingLeftHand = true;
        }
        else
        {
            HideLeftHand();
            showingLeftHand = false;
        }
    }
    private void ShowLeftHand()
    {
        leftHand.SetActive(true);
    }
    private void HideLeftHand()
    {
        leftHand.SetActive(false);
    }

    public void Spacebar()
    {
        mouseVisible = !mouseVisible;
        playerController.SetCanMove(!mouseVisible);
        playerController.SetCursorVisible(mouseVisible);
    }

    private void CheckInput()
    {
        //if (!detectInput || !hasAltair) return;

        if (!detectInput) return;
        //Toggle mouse enable
        if (Input.GetKeyDown(KeyCode.Space) /*&& showingRightHand*/)
        {
            Spacebar();
        }

        if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            playerSound.PlayPasos(true);
        }
        else if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D))
        {
            playerSound.PlayPasos(false);
        }

        if(mouseVisible && playerSound.playingPasos)
        {
            playerSound.PlayPasos(false);
        }

        //if (!hasAltair) return;
        //Right hand
        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    if (showingRightHand)
        //    {
        //        //if (!altair.passedHermeticTest) return;
        //        //showingRightHand = false;
        //    }
        //    else
        //    {
        //        showingRightHand = true;
        //    }
        //    SetRightHandVisible(true); //SetRightHandVisible(showingRightHand);
        //}
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    if (showingLeftHand)
        //    {
        //        showingLeftHand = false;
        //    }
        //    else
        //    {
        //        showingLeftHand = true;
        //    }
        //    SetLeftHandVisible(showingLeftHand);
        //    CheckLeftHandState();
        //}
    }
    public void CheckLeftHandState()
    {
        if (leftHand == null) return;
        switch(lHand)
        {
            case LeftHandState.None:
                pruebaHermeticidad.SetActive(false);
                gasHolding.SetActive(false);
                return;
            case LeftHandState.HermeticTest:
                pruebaHermeticidad.SetActive(true);
                return;
            case LeftHandState.HoldGas:
                gasHolding.SetActive(true);
                zonda.SetActive(false);
                return;
            case LeftHandState.Zonda:
                gasHolding.SetActive(false);
                zonda.SetActive(true);
                return;
        }
    }
    public void SetInputState(bool state)
    {
        if(state)
        {
            detectInput = true;
        }
        else
        {
            detectInput = false;
        }

    }
}
