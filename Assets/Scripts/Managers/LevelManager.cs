using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    public UnityEvent OnLoadedOutsideScene;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void LoadInsideTrailerScene()
    {
        SceneManager.LoadScene("1_Inside", LoadSceneMode.Single);
    }

    public void LoadOutsideScene()
    {
        SceneManager.LoadScene("2_Outside", LoadSceneMode.Single);
        OnLoadedOutsideScene?.Invoke();
    }

    public void LoadScoreScene()
    {
        SceneManager.LoadScene("3_ScoreScreen", LoadSceneMode.Single);
    }
}
