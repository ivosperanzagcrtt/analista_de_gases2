using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModeManager : MonoBehaviour
{
    public enum GameMode {Desktop, VR};
    public GameMode gameMode;

    private void Start()
    {
        gameMode = GameMode.Desktop;
    }
}
