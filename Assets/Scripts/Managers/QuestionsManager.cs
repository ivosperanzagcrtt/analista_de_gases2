using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using StarterAssets;
using UnityEngine.Events;

public class QuestionsManager : MonoBehaviour
{
    public static QuestionsManager Instance { get; private set; }
    LanguageManager language;
    QuestionLoggerManager questionLogger;
    ScoreManager scoreManager;
    PlayerManager playerControls;
    PlayerSoundManager playerSounds;
    [SerializeField] CanvasGroup canvasGroup;
    FirstPersonController player;
    WarningMessages alerts;

    //Texts
    [SerializeField] TextMeshProUGUI questionText;
    [SerializeField] TextMeshProUGUI answer1;
    [SerializeField] TextMeshProUGUI answer2;
    [SerializeField] TextMeshProUGUI answer3;
    [SerializeField] TextMeshProUGUI answer4;

    //Buttons
    [SerializeField] Button answer1_Button;
    [SerializeField] Button answer2_Button;
    [SerializeField] Button answer3_Button;
    [SerializeField] Button answer4_Button;
    private int correctAnswer;

    //Listas de preguntas
    [SerializeField] private List<Question> poolDePreguntas = new List<Question>();
    [SerializeField] private List<Question> Q_Hermeticidad = new List<Question>();
    [SerializeField] private List<Question> Q_AlarmaBaja = new List<Question>();
    [SerializeField] private List<Question> Q_AlarmaTWA = new List<Question>();
    [SerializeField] private List<Question> Q_Calibration = new List<Question>();
    [SerializeField] private List<Question> Q_FallaBumpTest = new List<Question>();
    [SerializeField] private List<Question> Q_PreguntaCorazon = new List<Question>();
    //New Question April 2022
    [SerializeField] private List<Question> Q_PreHermetic = new List<Question>();


    private List<Question> primeraLista = new List<Question>();
    private List<Question> segundaLista = new List<Question>();

    //No editar manualmente
    private List<Question> currentList = new List<Question>();
    private int currentIndex = 0;

    //Events
    public UnityEvent OnQuestionPopup;
    public UnityEvent OnAnsweredCorrectly;
    public UnityEvent OnAnsweredIncorrectly;
    public UnityEvent OnFinishedAnsweringList;

    //Particular events
    public UnityEvent OnAnsweredHermeticQuestions;
    public UnityEvent OnAnsweredQ_AlarmaBaja;
    public UnityEvent OnAnsweredQ_AlarmaTWA;
    public UnityEvent OnAnsweredQ_Calibration;
    public UnityEvent OnAnsweredFallaBumpTest;
    public UnityEvent OnAnsweredAdentroDeBodega;
    public UnityEvent OnShowEspacioConfinadoImage;
    public UnityEvent OnAnsweredHeartQuestionInside;
    public UnityEvent OnAnsweredPreHermetic;

    //Testing purposes
    private bool cutIt = false;

    //Visuals
    [SerializeField] private Color incorrectColor;
    [SerializeField] private Color correctColor;
    [SerializeField] Image q1_VisualFeedback;
    [SerializeField] Image q2_VisualFeedback;
    [SerializeField] Image q3_VisualFeedback;
    [SerializeField] Image q4_VisualFeedback;

    //4 Question related
    [SerializeField] GameObject fourthQuestion;

    //Question logger related;
    Question currentQuestion;

    //Block from touching Altair buttons
    public bool isOpen = false;

    //Sounds
    private AudioSource audioSource;
    [SerializeField] private AudioClip incorrectoSound;
    [SerializeField] private AudioClip correctoSound;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        player = FirstPersonController.Instance;
        playerSounds = PlayerSoundManager.Instance;
        scoreManager = ScoreManager.Instance;
        questionLogger = QuestionLoggerManager.Instance;
        playerControls = PlayerManager.Instance;
        language = LanguageManager.Instance;
        alerts = WarningMessages.Instance;
        audioSource = GetComponent<AudioSource>();
        SetVisible(false); //Dejar esta
        answer1_Button.onClick.AddListener(delegate { CheckAnswer(1); });
        answer2_Button.onClick.AddListener(delegate { CheckAnswer(2); });
        answer3_Button.onClick.AddListener(delegate { CheckAnswer(3); });
        answer4_Button.onClick.AddListener(delegate { CheckAnswer(4); });
        FillRandomly();
        //PlayList(primeraLista);
    }

    private void PlayIncorrecto()
    {
        audioSource.clip = incorrectoSound;
        audioSource.Play();
    }
    private void PlayCorrecto()
    {
        audioSource.clip = correctoSound;
        audioSource.Play();
    }
    private void FillRandomly()
    {
        //Se espera que la cantidad de preguntas sean PAR.
        int cantidadTotal = poolDePreguntas.Count;
        ScrambleList(poolDePreguntas, cantidadTotal);
        for (int i = 0; i < poolDePreguntas.Count; i++)
        {
            if (i < cantidadTotal / 2)
            {
                //Primera
                primeraLista.Add(poolDePreguntas[i]);
            }
            else
            {
                //Segunda lista
                segundaLista.Add(poolDePreguntas[i]);
            }
        }
        poolDePreguntas.Clear();
    }
    private void CheckAnswer(int givenAnswer)
    {
        if(questionLogger == null)
        {
            questionLogger = QuestionLoggerManager.Instance;
        }
        LogAnswer(givenAnswer);
        if (givenAnswer == correctAnswer)
        {
            PlayCorrecto();
            questionLogger.AddToLog(language.GetFromCustomSheet("L_CORRECT")/*"<color=green>Se respondió correctamente.</color>"*/);
            questionLogger.AddToLog(" ");
            ShowCorrectVisually(givenAnswer);
            if (scoreManager == null)
            {
                Debug.LogError("Score Manager not found! Did you start the game in the first scene?"); SetVisible(false); return;
            }
            //scoreManager.SumarPuntaje(1);
            OnAnsweredCorrectly?.Invoke();
            //GoToNextQuestion();
        }
        else
        {
            PlayIncorrecto();
            questionLogger.AddToLog(language.GetFromCustomSheet("L_INCORRECT")/*<color=red> Se respondió de manera erronea.</color>*/);
            questionLogger.AddToLog(" ");
            LogIncorrectlyAnswered(givenAnswer);
            //questionLogger.AddToIncorrectlyAnsweredLog("Respuesta usuario: " + currentQuestion.);
            ShowIncorrectVisually(givenAnswer);
            if (scoreManager == null)
            {
                Debug.LogError("Score Manager not found! Did you start the game in the first scene?"); SetVisible(false); return;
            }
            scoreManager.RestarPuntaje(5);
            OnAnsweredIncorrectly?.Invoke();
        }
        //GoToNextQuestion();

    }

    private void LogIncorrectlyAnswered(int givenAswer)
    {
        questionLogger.AddToIncorrectlyAnsweredLog(" ");
        questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet("L_INCORRECT")/*"<color=red> Se respondió de manera erronea.</color>"*/);
        switch (givenAswer)
        {
            case 1:
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet("L_SE_RESP")/*"Se respondió: "*/);
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet(currentQuestion.name + "_1"));/*(currentQuestion.answer_1);*/
                break;
            case 2:
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet("L_SE_RESP")/*"Se respondió: "*/);
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet(currentQuestion.name + "_2")); ;
                break;
            case 3:
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet("L_SE_RESP")/*"Se respondió: "*/);
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet(currentQuestion.name + "_3")); ;
                break;
            case 4:
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet("L_SE_RESP")/*"Se respondió: "*/);
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet(currentQuestion.name + "_4"));
                break;
            default:
                Debug.Log("Unknown Answer");
                break;
        }
        LogCorrectAnswerAfterIncorrect();
    }

    private void LogCorrectAnswerAfterIncorrect()
    {
        questionLogger.AddToIncorrectlyAnsweredLog("<color=green>"+language.GetFromCustomSheet("L_HOPED_ANS") +"</color>");
        switch (correctAnswer)
        {
            case 1:
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet(currentQuestion.name + "_1")/*currentQuestion.answer_1*/);
                break;
            case 2:
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet(currentQuestion.name + "_2")/*currentQuestion.answer_2*/);
                break;
            case 3:
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet(currentQuestion.name + "_3")/*currentQuestion.answer_3*/);
                break;
            case 4:
                questionLogger.AddToIncorrectlyAnsweredLog(language.GetFromCustomSheet(currentQuestion.name + "_4")/*currentQuestion.answer_4*/);
                break;
            default:
                Debug.Log("Unknown Answer");
                break;
        }
        questionLogger.AddToIncorrectlyAnsweredLog("-----------------------------------------------");
    }

    private void ShowIncorrectVisually(int index)
    {
        switch(index)
        {
            case 1:
                q1_VisualFeedback.color = incorrectColor;
                q1_VisualFeedback.enabled = true;
                StartCoroutine(waitAndDisableImage(q1_VisualFeedback, 0.5f));
                return;
            case 2:
                q2_VisualFeedback.color = incorrectColor;
                q2_VisualFeedback.enabled = true;
                StartCoroutine(waitAndDisableImage(q2_VisualFeedback, 0.5f));
                return;
            case 3:
                q3_VisualFeedback.color = incorrectColor;
                q3_VisualFeedback.enabled = true;
                StartCoroutine(waitAndDisableImage(q3_VisualFeedback, 0.5f));
                return;
            case 4:
                q4_VisualFeedback.color = incorrectColor;
                q4_VisualFeedback.enabled = true;
                StartCoroutine(waitAndDisableImage(q4_VisualFeedback, 0.5f));
                return;
        }
    }

    private void ShowCorrectVisually(int index)
    {
        switch (index)
        {
            case 1:
                q1_VisualFeedback.color = correctColor;
                q1_VisualFeedback.enabled = true;
                StartCoroutine(waitAndDisableImage(q1_VisualFeedback, 0.5f));
                StartCoroutine(waitAndGoToNextQuestion(0.5f));
                return;
            case 2:
                q2_VisualFeedback.color = correctColor;
                q2_VisualFeedback.enabled = true;
                StartCoroutine(waitAndDisableImage(q2_VisualFeedback, 0.5f));
                StartCoroutine(waitAndGoToNextQuestion(0.5f));
                return;
            case 3:
                q3_VisualFeedback.color = correctColor;
                q3_VisualFeedback.enabled = true;
                StartCoroutine(waitAndDisableImage(q3_VisualFeedback, 0.5f));
                StartCoroutine(waitAndGoToNextQuestion(0.5f));
                return;
            case 4:
                q4_VisualFeedback.color = correctColor;
                q4_VisualFeedback.enabled = true;
                StartCoroutine(waitAndDisableImage(q4_VisualFeedback, 0.5f));
                StartCoroutine(waitAndGoToNextQuestion(0.5f));
                return;
        }
    }

    IEnumerator waitAndGoToNextQuestion(float time)
    {
        yield return new WaitForSeconds(time);
        GoToNextQuestion();
    }

    IEnumerator waitAndDisableImage(Image img, float time)
    {
        yield return new WaitForSeconds(time);
        img.enabled = false;
    }

    public void PlayList(List<Question> list)
    {
        //OnQuestionPopup?.Invoke();
        //currentList = list;
        //currentIndex = 0;
        //InitializeQuestion(currentList[currentIndex]);
        //SetVisible(true);
        StartCoroutine(waitAndPlayList(list));
    }

    private IEnumerator waitAndPlayList(List<Question> listOfQuestions)
    {
        yield return new WaitForSeconds(2f);
        OnQuestionPopup?.Invoke();
        currentList = listOfQuestions;
        currentIndex = 0;
        InitializeQuestion(currentList[currentIndex]);
        SetVisible(true);
        CheckCurrentQuestion();
    }

    private void CheckCurrentQuestion()
    {
        if (currentQuestion == null) return;
        if (currentQuestion.name == "E_0")
        {
            OnShowEspacioConfinadoImage?.Invoke();
        }
    }

    private void GoToNextQuestion()
    {
        if(currentIndex < (currentList.Count - 1))
        {
            currentIndex++;
            InitializeQuestion(currentList[currentIndex]);
        }
        else
        {
            SetVisible(false);
            OnFinishedAnsweringList?.Invoke();
            if(cutIt == false)
            {
                cutIt = true;
            }
            CheckQuestionListFinished();
        }
    }
    public void InitializeQuestion(Question q)  
    {
        currentQuestion = q;
        questionText.text = language.GetFromCustomSheet(q.name + "_T");//q.title;
        answer1.text = language.GetFromCustomSheet(q.name + "_1");//q.answer_1;
        answer2.text = language.GetFromCustomSheet(q.name + "_2");//q.answer_2;
        answer3.text = language.GetFromCustomSheet(q.name + "_3");//q.answer_3;
        if (q.answer_4 == "")
        {
            fourthQuestion.SetActive(false);
        }
        else
        {
            answer4.text = language.GetFromCustomSheet(q.name + "_4");//q.answer_4;
            fourthQuestion.SetActive(true);
        }
        correctAnswer = q.correctAnswer;
        LogCurrentQuestion();
    }

    private void LogCurrentQuestion()
    {
        questionLogger.AddToLog(" ");
        questionLogger.AddToLog("<color=#FFF5EE>------------------------------------</color>");
        questionLogger.AddToLog(" ");
        questionLogger.AddToLog("<color=#800080>"+/*Pregunta:*/ language.GetFromCustomSheet("L_QUESTION") + language.GetFromCustomSheet(currentQuestion.name + "_T")/*currentQuestion.title*/+ "</color>");
        questionLogger.AddToLog(" ");
    }

    private void LogAnswer(int givenAnswer)
    {
        switch(givenAnswer)
        {
            case 1:
                questionLogger.AddToLog(language.GetFromCustomSheet("L_ANSWERED")/*"Respondió: "*/ + language.GetFromCustomSheet(currentQuestion.name + "_1")/*currentQuestion.answer_1*/);
                questionLogger.AddToLog(" ");
                break;
            case 2:
                questionLogger.AddToLog(language.GetFromCustomSheet("L_ANSWERED")/*"Respondió: "*/ + language.GetFromCustomSheet(currentQuestion.name + "_2"));
                questionLogger.AddToLog(" ");
                break;
            case 3:
                questionLogger.AddToLog(language.GetFromCustomSheet("L_ANSWERED")/*"Respondió: "*/ + language.GetFromCustomSheet(currentQuestion.name + "_3"));
                questionLogger.AddToLog(" ");
                break;
            case 4:
                questionLogger.AddToLog(language.GetFromCustomSheet("L_ANSWERED")/*"Respondió: "*/ + language.GetFromCustomSheet(currentQuestion.name + "_4"));
                questionLogger.AddToLog(" ");
                break;
            default:
                questionLogger.AddToLog("Respuesta no encontrada.");
                questionLogger.AddToLog(" ");
                break;
        }
    }

    public void SetVisible(bool state)
    {
        if(!playerControls)
        {
            playerControls = PlayerManager.Instance;
        }
        if (player == null)
        {
            player = FirstPersonController.Instance;
        }

        if (player == null) return;
        if(!playerSounds)
        {
            playerSounds = PlayerSoundManager.Instance;
            playerSounds.PlayPasos(false);
        }

        if (state) // Visible
        {
            isOpen = true;
            //playerControls.Spacebar();
            playerControls.detectInput = false;
            player.SetCanMove(false);
            player.SetCursorVisible(true);
            //Cursor.lockState = CursorLockMode.None;
            canvasGroup.alpha = 1f;
            canvasGroup.blocksRaycasts = true;
            canvasGroup.interactable = true;
        }
        else //Invisible 
        {
            isOpen = false;
            playerControls.detectInput = true;
            if (playerControls.isOutside)
            {
                player.SetCanMove(true);
                player.SetCursorVisible(false);
            }
            else
            {
                player.SetCanMove(false);
                player.SetCursorVisible(true);
            }
            //Cursor.lockState = CursorLockMode.Locked;
            canvasGroup.alpha = 0f;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
        }
    }
    public void ScrambleList<T>(List<T> list, int shuffleAccuracy)
    {
        for (int i = 0; i < shuffleAccuracy; i++)
        {
            int randomIndex = Random.Range(1, list.Count);

            T temp = list[randomIndex];
            list[randomIndex] = list[0];
            list[0] = temp;
        }
    }

    private void CheckQuestionListFinished()
    {
        if(currentList == Q_Hermeticidad)
        {
            OnAnsweredHermeticQuestions?.Invoke();
        }
        else if(currentList == Q_AlarmaBaja)
        {
            OnAnsweredQ_AlarmaBaja?.Invoke();
        }
        else if (currentList == Q_AlarmaTWA)
        {
            OnAnsweredQ_AlarmaTWA?.Invoke();
        }
        else if (currentList == Q_Calibration)
        {
            OnAnsweredQ_Calibration?.Invoke();
        }
        else if( currentList == Q_FallaBumpTest)
        {
            OnAnsweredFallaBumpTest?.Invoke();
        }
        else if(currentList.ToString() == "F_Adentro_Bodega")
        {
            OnAnsweredAdentroDeBodega?.Invoke();
        }
        else if(currentList == Q_PreguntaCorazon)
        {
            OnAnsweredHeartQuestionInside?.Invoke();
        }
        else if(currentList == Q_PreHermetic)
        {
            OnAnsweredPreHermetic?.Invoke();
        }
    }

    public void PlayHermeticidadQuestions()
    {
        PlayList(Q_Hermeticidad);
    }

    public void PlayAlarmaBajaQuestions()
    {
        PlayList(Q_AlarmaBaja);
    }

    public void PlayAlarmaTWAQuestions()
    {
        PlayList(Q_AlarmaTWA);
    }

    public void PlayCalibrationQuestions()
    {
        PlayList(Q_Calibration);
    }

    public void PlayFallBumpTestQuestions()
    {
        PlayList(Q_FallaBumpTest);
    }

    public void PlayPreguntasCorazon()
    {
        PlayList(Q_PreguntaCorazon);
    }

    public void PlayPreHermetic()
    {
        PlayList(Q_PreHermetic);
    }
}
