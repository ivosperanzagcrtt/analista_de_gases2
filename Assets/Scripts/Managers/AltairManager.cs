using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AltairManager : MonoBehaviour
{
    public static AltairManager Instance { get; private set; }
    private AltairSoundLightManager lightSoundManager;
    private PlayerManager playerManager;
    private Canvas canvas;

    //Input
    [SerializeField] public BotonAbajo botonAbajo;
    [SerializeField] public BotonOnOff botonOnOff;
    [SerializeField] public BotonArriba botonArriba;

    //General
    public bool isOn;
    public float batteryPercentage;
    public float shutdownTime = 5f;
    public float heldDownTimer = 0f;
    public float heldDownRightButtonTimer = 0f;
    public bool isCharging = false;
    public float loadedPercentage = 0f;
    private bool doneLoadingBootUp = false;
    public bool canBeTurnedOff = false; 
    //Events
    public UnityEvent OnAltairTurnOn;
    public UnityEvent OnAltairTurnOff;
    public UnityEvent OnTurnOffHeldDown;
    public UnityEvent OnReleasedTurnOff;
    public UnityEvent OnGrabbedFromChargeStation;
    public UnityEvent OnStartLoading;
    public UnityEvent OnLoadedPercentage;
    public UnityEvent OnHeldDownRightButtonForThreeSeconds;

    //LOADING EVENTS
    public UnityEvent Reached_TipoDeGasPentano;
    public UnityEvent Reached_AlarmaBaja;
    public UnityEvent Reached_AlarmaAlta;
    public UnityEvent Reached_AlarmaStel;
    public UnityEvent Reached_AlarmaTWA;
    public UnityEvent Reached_BotellaCalibracion;
    public UnityEvent Reached_FechaScreen;
    public UnityEvent Reached_UltCalibracionScreen;
    public UnityEvent Reached_FaltaCalibrar;
    public UnityEvent Reached_PorFavorEspere;
    public UnityEvent FinishedLoading;

    //Held down auxiliars
    private bool holdingDownRightButton = false;

    //On Off Auxiliars
    private bool holdingDownOnButton = false;

    //Memory
    public GameObject altairModel;
    public Transform startingRotation;
    public Transform hermeticTestRotation;
    public bool passedHermeticTest = false;
    public bool passedBumpTest = false;
    public bool passedAireLimpio = false;

    //Inventory
    public bool inHand = false;
    public bool canMoveToBelt = false;

    //Gas Tube Related
    public Transform entradaGas;

    //Other
    public bool stopLoadingToAnswer = false;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        playerManager = PlayerManager.Instance;
        lightSoundManager = AltairSoundLightManager.Instance;
        canvas = GetComponent<Canvas>();
        canvas.worldCamera = playerManager.mainCamera;

        //Boton Flecha Abajo
        botonAbajo.OnBotonAbajo.AddListener(BotonAbajo);

        //Boton On off
        botonOnOff.OnBotonOnOff.AddListener(BotonOnOff);
        botonOnOff.OnReleaseOnOff.AddListener(ReleaseOnOff);

        //Boton Flecha Arriba
        botonArriba.OnBotonArriba.AddListener(BotonArriba);
        botonArriba.OnReleaseBottonArriba.AddListener(ReleaseBotonArriba);

        canBeTurnedOff = false;
    }

    private void Update()
    {
        if (!isOn) return;
        TurnOffCheck(); 
        HeldDownRightButtonCheck();
    }

    private void BotonAbajo()
    {
        if (isCharging) return;
    }

    private void BotonOnOff()
    {
        if (isCharging) return;
        holdingDownOnButton = true;
        TurnOnOff();
    }

    private void ReleaseOnOff()
    {
        if (isCharging) return;
        holdingDownOnButton = false;
        heldDownTimer = 0f;
    }

    private void BotonArriba()
    {
        if (isCharging) return;
        holdingDownRightButton = true;
    }

    private void ReleaseBotonArriba()
    {
        if (isCharging) return;
        holdingDownRightButton = false;
        heldDownTimer = 0f;
    }

    private void TurnOnOff()
    {
        if (!isOn)
        {
            lightSoundManager.PlayStartingSequence();
            SetOnOff(true);
        }
    }
    public void SetOnOff(bool state)
    {
        if(state)
        {
            if(batteryPercentage >= 0f)
            {
                isOn = true;
                OnAltairTurnOn?.Invoke();
            }
            else
            {
                //Low battery alert
            }

        }
        else
        {
            isOn = false;
            OnAltairTurnOff?.Invoke();
        }
    }

    private void HeldDownRightButtonCheck()
    {
        if (holdingDownRightButton)
        {
            heldDownRightButtonTimer += Time.deltaTime;
            if (heldDownRightButtonTimer >= 3f)
            {
                OnHeldDownRightButtonForThreeSeconds?.Invoke();
                holdingDownRightButton = false;
                return;
            }
        }
        else
        {
            heldDownRightButtonTimer = 0f;
        }
    }

    private void TurnOffCheck()
    {
        //if (!passedHermeticTest && doneLoadingBootUp) return; //No poder apagar antes de que se haga la prueba por coroutines.
        if (!canBeTurnedOff) return;
        if(holdingDownOnButton)
        {
            AltairSoundLightManager.Instance.PlayOffSound();
            heldDownTimer += Time.deltaTime;
            if (heldDownTimer >= 1f)
            {
                OnTurnOffHeldDown?.Invoke();
                if (heldDownTimer >= 5f)
                {
                    SetOnOff(false);
                    heldDownTimer = 0f;
                }
            }
        }
        else
        {
            AltairSoundLightManager.Instance.StopOffSound();
            OnReleasedTurnOff?.Invoke();
            heldDownTimer = 0f;
        }
    }

    public void ChangeToHermeticTestRotation()
    {
        altairModel.transform.rotation = hermeticTestRotation.rotation;
    }

    public void ChangeToStartingRotation()
    {
        altairModel.transform.rotation = startingRotation.rotation;
    }

    public void GrabAltair()
    {
        isCharging = false;
        OnGrabbedFromChargeStation?.Invoke();
        //Show message to Open altair
        playerManager.showingRightHand = true;
        playerManager.SetRightHandVisible(true);
    }

    public void MoveToHand()
    {
        if (inHand) return;
        inHand = true;
        altairModel.transform.SetParent(playerManager.altairHandPosition);
        altairModel.transform.localRotation = Quaternion.identity;
        altairModel.transform.localPosition = Vector3.zero;
        altairModel.transform.localScale = Vector3.one;
    }

    public void MoveToBelt()
    {
        //if (!canMoveToBelt) return;
        inHand = false;
        altairModel.transform.SetParent(playerManager.toolBelt);
        altairModel.transform.localRotation = Quaternion.identity;
        altairModel.transform.localPosition = Vector3.zero;
        altairModel.transform.localScale = Vector3.one;
    }

    public void StartLoading()
    {
        OnStartLoading?.Invoke();
        StartCoroutine(Loading());

    }

    IEnumerator Loading()
    {
        while(!doneLoadingBootUp)
        {
            while(stopLoadingToAnswer)
            {
                yield return null;
            }
            yield return new WaitForSeconds(.75f);
            loadedPercentage++;
            OnLoadedPercentage?.Invoke();
            LoadingEvents();
            yield return null;
        }
    }
    
    private void LoadingEvents()
    {
        if (doneLoadingBootUp) return;
        switch(loadedPercentage)
        {
            case 22:
                Reached_TipoDeGasPentano?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 30:
                Reached_AlarmaBaja?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 36:
                Reached_AlarmaAlta?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 43:
                Reached_AlarmaStel?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 50:
                Reached_AlarmaTWA?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 56:
                Reached_BotellaCalibracion?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 61:
                Reached_FechaScreen?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 66:
                Reached_UltCalibracionScreen?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 71:
                Reached_FaltaCalibrar?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 75:
                Reached_PorFavorEspere?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
            case 100:
                doneLoadingBootUp = true;
                FinishedLoading?.Invoke();
                lightSoundManager.PlayGenericBeep();
                break;
        }
    }

}