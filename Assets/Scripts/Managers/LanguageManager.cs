﻿using Polyglot;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LanguageManager : MonoBehaviour, ILocalize
{
    public DownloadPolyglotSheetButton d;
    public static LanguageManager Instance { get; private set; }
    public UnityEvent OnLanguageChange;
    public Language currentLanguage;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        DontDestroyOnLoad(this);
        //d.OnClick();
        currentLanguage = Localization.Instance.SelectedLanguage;
        Localization.Instance.AddOnLocalizeEvent(this);
    }

    public void OnLocalize()
    {
        currentLanguage = Localization.Instance.SelectedLanguage;
        OnLanguageChange?.Invoke();
        Debug.Log("Current language: " + currentLanguage.ToString());
    }

    public string GetFromCustomSheet(string key)
    {
        return Localization.Get(key);
    }
}
