using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenuAttribute(menuName = "Pregunta")]
public class Question : ScriptableObject
{
    public string title = "�?";
    public string answer_1;
    public string answer_2;
    public string answer_3;
    public string answer_4;
    public int correctAnswer;

    //private void Awake()
    //{
    //    string a1 = answer_1;
    //    string a2 = answer_2;
    //    string a3 = answer_2;

    //    for(int i = 0; i < 3; i++)
    //    {
    //        int rand = Random.Range(1,4);

    //    }
    //}
}
