﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageFlagManager : MonoBehaviour
{
    public List<LanguageFlag> flags = new List<LanguageFlag>();

    private void Start()
    {
        for(int i = 0; i < flags.Count; i++)
        {
            flags[i].button.onClick.AddListener(delegate { StartCoroutine(waitFrame()); });
        }
    }

    private IEnumerator waitFrame()
    {
        yield return new WaitForEndOfFrame();
        CheckFlag();
    }

    private void CheckFlag()
    {
        for(int i = 0; i < flags.Count; i++)
        {
            flags[i].CheckFlag();
        }
    }
}
