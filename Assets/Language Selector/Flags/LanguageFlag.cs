﻿using Polyglot;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageFlag : MonoBehaviour
{
    public Image flag;
    public Button button;
    public Polyglot.Language lang;

    int id = -1;
    void Start()
    {
        flag = GetComponent<Image>();
        button = GetComponent<Button>();
        id = transform.GetSiblingIndex();
        button.onClick.AddListener(ChangeLanguage);
        CheckFlag();
    }

    private void ChangeLanguage()
    {
        if(lang != Localization.Instance.SelectedLanguage)
        {
            Localization.Instance.SelectLanguage(id);
        }
    }

    public void CheckFlag()
    {
        if (lang == Localization.Instance.SelectedLanguage)
        {
            EnableImage(true);
        }
        else
        {
            EnableImage(false);
        }
    }

    private void EnableImage(bool state)
    {
        if(state)
        {
            flag.color = new Color(flag.color.r, flag.color.g, flag.color.b, 1f);
        }
        else
        {
            flag.color = new Color(flag.color.r, flag.color.g, flag.color.b, 0.25f);
        }
    }
}
