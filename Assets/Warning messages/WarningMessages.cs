﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WarningMessages : MonoBehaviour {

    public static WarningMessages Instance { get; private set; }
    private string lastMessage;


    public TextMeshProUGUI messageText;
    public RectTransform messageTextContainer;

    private Vector3 initialYPosition;

    private Queue<string> queuedMessages;
    private bool readyForNextMessage;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        queuedMessages = new Queue<string>();
        initialYPosition = new Vector3(0,messageTextContainer.sizeDelta.y,0);
        readyForNextMessage = true;
    }
	
	// Update is called once per frame
	void Update () {
        if(queuedMessages != null){
            if (queuedMessages.Count > 0)
            {
                if (readyForNextMessage)
                {
                    readyForNextMessage = false;
                    StartCoroutine(ShowMessaage());
                }
            }
        }


    }

    IEnumerator ShowMessaage()
    {
        messageText.text = queuedMessages.Peek();

        for (float i = 0; i < 1; i += Time.deltaTime)
        {
            messageTextContainer.anchoredPosition = Vector3.Lerp(initialYPosition, Vector3.zero, i);
            yield return null;
        }
        messageTextContainer.anchoredPosition = Vector3.zero;
        yield return new WaitForSeconds(3f);


        for (float i = 0; i < 1; i += Time.deltaTime)
        {
            messageTextContainer.anchoredPosition = Vector3.Lerp(Vector3.zero, initialYPosition, i);
            yield return null;
        }
        messageTextContainer.anchoredPosition = initialYPosition;
        queuedMessages.Dequeue();
        readyForNextMessage = true;
    }


    public void ShowWarning(string newMessage)
    {
        if (!queuedMessages.Contains(newMessage))
        {
            queuedMessages.Enqueue(newMessage);
            lastMessage = newMessage;
        }
        
    }

 
}
